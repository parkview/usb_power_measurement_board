/* #######################################################
*
* Adjustable USB Power and Measurement Board
* Parkview 2023  Writen for PCB Version: 0.7
*
* A simple test of the ST7789 SPI based LCD 240x240px screen
*
*
* Todo's:
---------
Done: test outputting serial text over USB and Serial port
Done: test SSD1351 screen

*/

#include <Arduino.h>
#include <Arduino_GFX_Library.h>

// SPI ADC pins
#define HSPI_SCLK 13 // SPI Clock
#define HSPI_MISO 16 // SPI MISO
#define HSPI_MOSI 14 // SPI MOSI
#define HSPI_CS 15   // SPI ADC Chip Select
#define HSPI_DC 2    // SPI D/C for OLED
#define HSPI_CS1 1   // SPI OLED Screen CS
#define HSPI_RST -1  // not using a Reset pin (tied high)
#define ledRunPin 46 // program running LED
#define LCD_WIDTH 128
#define LCD_HEIGHT 128

const uint8_t BUZZPWM_CHANNEL = 0;    // ESP32 has 16 channels which can generate 16 independent waveforms
const int BUZZPWM_FREQ = 4000;        // Recall that Arduino Uno is ~490 Hz. Official ESP32 example uses 5,000Hz
const uint8_t BUZZPWM_RESOLUTION = 8; // We'll use same resolution as Uno (8 bits, 0-255) but ESP32 can go up to
const int BUZZMAX_DUTY_CYCLE = (int)(pow(2, BUZZPWM_RESOLUTION) - 1);
const uint8_t buzzerPin = 6;            // coming in Version 0.6. Not yet in V0.5
const uint8_t LEDPWRPWM_CHANNEL = 2;    // ESP32 has 16 channels which can generate 16 independent waveforms
const int LEDPWRPWM_FREQ = 5000;        // Recall that Arduino Uno is ~490 Hz. Official ESP32 example uses 5,000Hz
const uint8_t LEDPWRPWM_RESOLUTION = 8; // We'll use same resolution as Uno (8 bits, 0-255) but ESP32 can go up to
const int LEDPWRMAX_DUTY_CYCLE = (int)(pow(2, LEDPWRPWM_RESOLUTION) - 1);
const int DELAY_MS = 4; // delay between fade increments
int _ledFadeStep = 5;   // amount to fade per loop

// SSD1351 SPI 128x128 OLED screen init
Arduino_DataBus *bus = new Arduino_SWSPI(HSPI_DC /* DC */, HSPI_CS1 /* CS */, HSPI_SCLK /* SCK */, HSPI_MOSI /* MOSI */, HSPI_MISO /* MISO */);
Arduino_GFX *gfx = new Arduino_SSD1351(bus, HSPI_RST /* RST */, 0 /* rotation */);

void soundBuzzer(uint16_t buzzDelay)
{
  // sound the buzzer for specified length of time
  ledcWrite(BUZZPWM_CHANNEL, 20);
  delay(buzzDelay); // mSec delay
  ledcWrite(BUZZPWM_CHANNEL, 0);
}

void setup()
{
  Serial.begin(115200);
  delay(500); // wait till serial is online
  Serial.println("serial ready");
  // Buzzer setup
  ledcSetup(BUZZPWM_CHANNEL, BUZZPWM_FREQ, BUZZPWM_RESOLUTION);
  ledcAttachPin(buzzerPin, BUZZPWM_CHANNEL);
  Serial.println("buzzer ready");
  soundBuzzer(200);
  soundBuzzer(200);
  // setup Power LED
  ledcSetup(LEDPWRPWM_CHANNEL, LEDPWRPWM_FREQ, LEDPWRPWM_RESOLUTION);
  ledcAttachPin(ledRunPin, LEDPWRPWM_CHANNEL);
  ledcWrite(LEDPWRPWM_CHANNEL, 10);
  Serial.println("0");

  // OLED Setup
  gfx->begin();
  gfx->fillScreen(BLACK);

#ifdef GFX_BL
  pinMode(GFX_BL, OUTPUT);
  digitalWrite(GFX_BL, HIGH);
#endif

  Serial.println("1");
  gfx->setCursor(10, 10);
  gfx->setTextColor(RED);
  gfx->println("Hello World!");
  delay(2000); // 5 seconds
}

void loop()
{
  // set inital board states
  gfx->setCursor(random(gfx->width()), random(gfx->height()));
  gfx->setTextColor(random(0xffff), random(0xffff));
  gfx->setTextSize(random(2) /* x scale */, random(2) /* y scale */, random(2) /* pixel_margin */);
  // gfx->setTextSize(random(6) /* x scale */, random(6) /* y scale */, random(2) /* pixel_margin */);
  gfx->println("Hello World!");
  delay(1); // 1 mSec
}
