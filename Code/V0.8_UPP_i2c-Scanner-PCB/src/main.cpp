
/*===========================================================================
  Title: I2C Scanner, Adapted for the v0.8 USB Protected Power Board - 2023-07-30
    - Repository: https://gist.github.com/AustinSaintAubin/dc8abb2d168f5f7c27d65bb4829ca870
  
  Version: 8
  Date: 2018 / 02 / 17
  Author: Austin St. Aubin
  Email: AustinSaintAubin@gmail.com
  
  Description: 
    Scans for i2c Devices
  
   
  Notes:
  
    This sketch tests the standard 7-bit addresses
    Devices with higher bit address might not be seen properly.
    
  Expected Results:
   We should get back: 0x20=PCA9535 GPIO Expander, 0x2E=AD5247 100K Dig Pot, 0x2F=MCP4019-5K Dig Pot, 0x51=PCF85063 RTC

  ============================================================================= */

// [# Preprocessor Statements #]
 #define OLED_DISPLAY false // Enable OLED Display
 #define OLED_DISPLAY_SSD1306  // OLED Display Type: SSD1306(OLED_DISPLAY_SSD1306) / 

// [ Included Library's ]
#include <Wire.h>
// Initialize the OLED display using I2C
#ifdef OLED_DISPLAY_SSD1306
  #include "SSD1306.h"   // alias for `#include "SSD1306Wire.h"`
#elif defined OLED_DISPLAY_SH1106
  #include "SH1106.h"  // alias for `#include "SH1106Wire.h"`
#elif !OLED_DISPLAY
  #warning "OLED Display Disabled"
#else
  #error "Undefined OLED Display Type"
#endif

// [ Global Pin Constants / Varables ]
const uint8_t I2C_SDA_PIN = 21; //SDA;  // ESP32-S3 WROOM32 i2c SDA Pin
const uint8_t I2C_SCL_PIN = 47; //SCL;  // ESP32-S3 WROOM32 i2c SCL Pin
const u8_t powerLEDPin = 46;     // LED Run/Power
const uint8_t DISPLAY_WIDTH = 128;
const uint8_t DISPLAY_HEIGHT = 64;
const uint32_t FREQ = 100000;
// [ Global Classes ]
#ifdef OLED_DISPLAY_SSD1306
  SSD1306  display(0x3c, I2C_SDA_PIN, I2C_SCL_PIN);
#elif defined OLED_DISPLAY_SH1106
  SH1106 display(0x3c, I2C_SDA_PIN, I2C_SCL_PIN);
#endif


void setup()
{
  // Serial
  Serial.begin(115200);
  //Serial.setTxTimeoutMS(10);    // might help a ESP32-C3
  Serial.setDebugOutput(true);    // needed for the ESP32-S3-Mini
  delay(3000);
  // setup Test LEDs
  //pinMode(21, OUTPUT);  // 3.3V for sensors
  pinMode(powerLEDPin, OUTPUT);   // Green Power LED
  //pinMode(15, OUTPUT);  // 
  digitalWrite(powerLEDPin, HIGH);
//  digitalWrite(21, HIGH);
  //digitalWrite(15, HIGH);
  Serial.println("\ni2C Scanner");
  //Wire.begin(I2C_SDA_PIN, I2C_SCL_PIN);
  Wire.begin((int)I2C_SDA_PIN, I2C_SCL_PIN, FREQ);
  //Serial.println("Sc");

  // OLED
  #if OLED_DISPLAY
    display.init();
    display.flipScreenVertically();
    display.setContrast(255);
    display.setFont(ArialMT_Plain_10);
    display.setColor(WHITE);
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(DISPLAY_WIDTH, 0, "v8");
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "I2C Scanner w/ OLED");
    display.drawLine(0, 12, DISPLAY_WIDTH, 12);
    display.display();
  Serial.println("OLED begun");
  #endif
    // turn on and ini the LIS2DH12 IC

  
  Serial.println("Setup done");
}


void loop()
{
  byte error, address;
  int nDevices;

  Serial.println("Scanning...");
  digitalWrite(8, HIGH);
  #if OLED_DISPLAY
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 14, "Scanning...");
    display.display();
  #endif
 
  nDevices = 0;
  for(address = 1; address < 127; address++ ) {
    // The i2c_scanner uses the return value of
    // the Write.endTransmission to see if
    // a device did acknowledge the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0) {
      nDevices++;
      
      Serial.print("Device found @ 0x");
      if (address<16) 
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
      
      #if OLED_DISPLAY
        display.drawString(((nDevices -1) % 4) * 32, 23 + (round((nDevices -1) %4) *10), (String)nDevices + ":x" + String(address, HEX));
        display.display();
      #endif
    }
    else if (error==4) 
    {
      Serial.print("Unknow error @ 0x");
      if (address<16) 
        Serial.print("0");
      Serial.println(address,HEX);

      #if OLED_DISPLAY
        display.drawString(((nDevices -1) % 4) * 32, 23 + (round((nDevices -1) %4) *10), "!:x" + String(address, HEX));
        display.display();
      #endif
    }    
  }
  if (nDevices == 0) {
    Serial.println("No I2C devices found\n");
    
    #if OLED_DISPLAY
        display.drawString(0, 23 , "No I2C devices found");
        display.display();
    #endif
  } else {
    Serial.print("No. of i2C devices found: ");
    Serial.println(nDevices);
    Serial.println("done\n");
    
    #if OLED_DISPLAY
        display.drawString(((nDevices -0) % 4) * 32, 23 + (round((nDevices -0) %4) *10), "done");
        display.display();
    #endif
  }
  digitalWrite(powerLEDPin, LOW);  // toggle the Power LED
  delay(2000);           // wait 2 seconds for next scan

  #if OLED_DISPLAY
    // Clear Bottom of Display
    display.setColor(BLACK);
    display.fillRect(0, 13, DISPLAY_WIDTH, DISPLAY_HEIGHT - 13);
    display.setColor(WHITE);
  #endif

}
