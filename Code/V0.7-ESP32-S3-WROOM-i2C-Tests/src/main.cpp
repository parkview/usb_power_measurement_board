/* #######################################################
*
* Adjustable USB Power and Measurement Board
* Parkview 2023  Written for PCB Version: 0.7
*
* view code at: https://gitlab.com/parkview/usb_power_measurement_board
*
*
* Todo's:
---------
* run a series of i2C chip tests to check the i2C bus out
*
*/

// #define OLED_Display SSD1306
#include <Arduino.h>
#include <Wire.h>
#include <PCF85063TP.h> // Seeed's RTC Library: https://github.com/Seeed-Studio/Grove_High_Precision_RTC_PCF85063TP
#include "PCAL9535A.h"  // add in header for the PCA9535 i2C GPIO port expander IC

#define NUM_RGBleds 20
#define LED_TYPE WS2812B
#define COLOR_ORDER GRB
#define BRIGHTNESS 15
#define RGB_PIN 17
#define ledRunPin 46 // program running LED
#define SDA 21
#define SCL 47
// PCA9535 Ports 0
#define usb1_EN 0 // turn on USB Micro input
#define pot5kAddress 0x2f
#define pot100kAddress 0x2e

PCD85063TP RTclock; // define a object of PCD85063TP class
PCAL9535A::PCAL9535A<TwoWire> gpio(Wire);

const uint8_t BUZZPWM_CHANNEL = 0;    // ESP32 has 16 channels which can generate 16 independent waveforms
const int BUZZPWM_FREQ = 4000;        // Recall that Arduino Uno is ~490 Hz. Official ESP32 example uses 5,000Hz
const uint8_t BUZZPWM_RESOLUTION = 8; // We'll use same resolution as Uno (8 bits, 0-255) but ESP32 can go up to
const int BUZZMAX_DUTY_CYCLE = (int)(pow(2, BUZZPWM_RESOLUTION) - 1);
const uint8_t buzzerPin = 6;            // coming in Version 0.6. Not yet in V0.5
const uint8_t LEDPWRPWM_CHANNEL = 2;    // ESP32 has 16 channels which can generate 16 independent waveforms
const int LEDPWRPWM_FREQ = 5000;        // Recall that Arduino Uno is ~490 Hz. Official ESP32 example uses 5,000Hz
const uint8_t LEDPWRPWM_RESOLUTION = 8; // We'll use same resolution as Uno (8 bits, 0-255) but ESP32 can go up to
const int LEDPWRMAX_DUTY_CYCLE = (int)(pow(2, LEDPWRPWM_RESOLUTION) - 1);
const int DELAY_MS = 4; // delay between fade increments
int _ledFadeStep = 5;   // amount to fade per loop

void printTime()
{
  RTclock.getTime();
  Serial.print(RTclock.hour, DEC);
  Serial.print(":");
  Serial.print(RTclock.minute, DEC);
  Serial.print(":");
  Serial.print(RTclock.second, DEC);
  Serial.print("  ");
  Serial.print(RTclock.year + 2000, DEC);
  Serial.print("-");
  Serial.print(RTclock.month, DEC);
  Serial.print("-");
  Serial.print(RTclock.dayOfMonth, DEC);
  Serial.print(" ");
  switch (RTclock.dayOfWeek)
  { // Friendly printout the weekday
  case MON:
    Serial.print("Monday");
    break;
  case TUE:
    Serial.print("Tuesday");
    break;
  case WED:
    Serial.print("Wednsday");
    break;
  case THU:
    Serial.print("Thursday");
    break;
  case FRI:
    Serial.print("Friday");
    break;
  case SAT:
    Serial.print("Saturday");
    break;
  case SUN:
    Serial.print("Sunday");
    break;
  }
  Serial.println(" ");
}

void soundBuzzer(uint16_t buzzDelay)
{
  // sound the buzzer for specified length of time
  ledcWrite(BUZZPWM_CHANNEL, 20);
  delay(buzzDelay); // mSec delay
  ledcWrite(BUZZPWM_CHANNEL, 0);
}

void setTime()
{
  // use this to set the current time into PFC85063 RTC
  RTclock.stopClock();
  RTclock.fillByYMD(2023, 1, 16); // May 27,2022
  RTclock.fillByHMS(12, 16, 00);  // 21:40 00"
  RTclock.fillDayOfWeek(MON);     // Friday
  RTclock.setTime();              // write time to the RTC chip
  RTclock.startClock();           // start the clock again
}

void write_5K(uint8_t reg)
{
  Serial.println("send 5K pot data");
  Wire.beginTransmission(pot5kAddress);
  Wire.write(reg);
  Wire.endTransmission();
  Serial.println("5K pot data written!");
}

uint8_t read_5K(void)
{
  Serial.println("read 5K pot data");
  Wire.beginTransmission(pot5kAddress);
  Wire.endTransmission();
  Wire.requestFrom(pot5kAddress, 1);
  uint8_t reg = Wire.read();
  Wire.endTransmission();
  return reg;
}

void write_100k(uint8_t reg)
{
  Serial.println("send 100K pot data");
  Wire.beginTransmission(pot100kAddress);
  Wire.write(reg);
  Wire.endTransmission();
  Serial.println("100K pot data written!");
}

uint8_t read_100k(void)
{
  Serial.println("read 100K pot data");
  Wire.beginTransmission(pot100kAddress);
  Wire.endTransmission();
  Wire.requestFrom(pot100kAddress, 1);
  uint8_t reg = Wire.read();
  Wire.endTransmission();
  return reg;
}

void gpioTest(void)
{
  // this should send an approx 1Khz on/off square wave on GPIO 0, to R20 near USB Micro input
  for (uint8_t i = 0; i < 100000; i++)
  {
    gpio.digitalWrite(0, LOW);
    // delay(1);
    gpio.digitalWrite(0, HIGH);
    // delay(1);
  }
}

void i2C5vTest(void)
{
  //  this will test the 5V i2C line for the 5K and 100K pots.  Scope the lines out at the RN1 if required
  // check the 5K pot
  uint8_t readout = read_5K();  // initial power up value = 63
  Serial.print("5K pot data: ");
  Serial.println(readout);
  write_5K(23);
  readout = read_5K();
  Serial.print("5K pot data: ");
  Serial.println(readout);
  // check the 100K pot
  readout = read_100k();   // initial power up value = 64
  Serial.print("100K pot data: ");
  Serial.println(readout);
  write_100k(39);
  readout = read_100k();
  Serial.print("100K pot data: ");
  Serial.println(readout);
}

void setup()
{
  Wire.begin(SDA, SCL);
  // Wire.setClock(400000);
  Serial.begin(115200);
  delay(5000); // wait till serial is online
  Serial.println("serial ready");
  // Buzzer setup
  ledcSetup(BUZZPWM_CHANNEL, BUZZPWM_FREQ, BUZZPWM_RESOLUTION);
  ledcAttachPin(buzzerPin, BUZZPWM_CHANNEL);
  Serial.println("buzzer ready");
  soundBuzzer(200);
  soundBuzzer(200);
  // setup Power LED
  ledcSetup(LEDPWRPWM_CHANNEL, LEDPWRPWM_FREQ, LEDPWRPWM_RESOLUTION);
  ledcAttachPin(ledRunPin, LEDPWRPWM_CHANNEL);
  ledcWrite(LEDPWRPWM_CHANNEL, 10);
  Serial.println("0");

  Serial.println("2");
  // setup PCA9535 GPIO Expander
  gpio.begin();            // use default address 0
  gpio.pinMode(0, OUTPUT); // USB1_EN - USB Micro (probe R20 with O'scope)
  Serial.println("4");
  RTclock.begin(); // initialise the RTC IC
  Serial.println("5");
   //setTime(); //  Use this to set date/time. Need to edit function with correct time first. Could allow setting via encoder

  // testVoltages();  // use this to output a 1-127 LDO resistor/voltage out data plot
  Serial.println("Setup finished");
}

void loop()
{
  // read out from PCF85063 RT Clock
  printTime();   // i2C 3.3V test
  Serial.println();
  // gpioTest();
  //i2C5vTest();   // i2C 5V test
  delay(900); // wait a bit so we can see the new data, before clearing the screen
}
