/* #######################################################
*
* Adjustable USB Power and Measurement Board
* Parkview 2022  Writen for PCB Version: 0.8
*
* view code at: https://gitlab.com/parkview/usb_power_measurement_board
* project mini-write up can be found at:
*
*
* Todo's:
---------
* instead of blanking out the entire screen after every loop iteration, just blank out the data or line that needs changing
* add a propper - more flexible menuing system. Need to change Sense measurements
* Done: Get WiFi AP mode working: See example: https://randomnerdtutorials.com/esp32-dht11-dht22-temperature-humidity-web-server-arduino-ide/
* TODO: get wifi connecting to local network, via Secret's file
* Could a STRUCT array hold both RGB LED GPIO and enable pin GPIO data?
Done: use a Button 2 to kick off data recording.  Use RE to select SPS/capture rate and length
Done: record data to a SD card.  Only works in SPI mode at around 1.1KSPS
* check nRF board and software and compare to my V0.5.  How to improve?
* set SW1 to be a Mode switch, where it changes menu's around:  0 = setup; 1 = data collection; 2 = SD card file management [save|replay|send to wifi]?
Done: fix display showing -0.520mA with nothing plugged in!
Done: test writing data to SD card using SD-SPI mode.  Get 1K SPS written to SD card via SPI mode.
* should I be measuring voltage loss across the sense resistor?  If the loss is too high, then change to a lower ohm resistor.
* Need to measure the sense + voltage dividers resistors better
* I seem to have a stable voltage out of INA180 + Vref, but ADC128 seems to give a 100-300+uA fluctuating voltage - WHY?  Seems to be the Noisy Vref.
Done: fix start mode.  LEDs and menu look ok, but really voltage & current are not being read correctly
* could cycle through imputs, looking for one with a voltage on it?  Maybe toggle this via webpage config?
Done: get input source menu selection going
Done: add in interupts for the 3 user switches, via the i2C expander
Done: get both SPI (OLED + ADC IC), working together
* get a scrolling current graph going along the bottom of the OLED display.  how to do this? Could use Button 3 for this mode?
Done: middle switch not working!
Done: check enableVoltagePaths - is current path and LEDs working ok?
Done: a huge multi second timeout for Serial output.  Need to detect that it's not plugged in and bypass serial outputs, see: https://github.com/espressif/arduino-esp32/issues/6983
* change over to a vTask based loop.  This will speed up menu and display etc
* record SDMMC data via my 8 ch. Logic Analyser to see whats wrong with 4 bit mode. <looks like it has to be hard wired to specific GPIO for 1-bit + 4-Bit to work>
* add a menu selection to set the kind of data averaging:  0, 4, 8, 16
Done: use SW1 to start/stop data recording to serial bus.  SW2 to record to SD card.  Display MODE along bottom of screen
* add a menu item to set data collection frequency: 100/sec, 500/sec, 1000/sec, 5000/sec, 10000/sec (how to deal with fast data?)
Done: have an option to transmit data via WiFi to remote PC.  Could use BlueTooth to set IP address? Or to transfer data as an option?  Could have webpage option to select destination: UDP; SD card; OLED
* how to find which variables are not being used?  Clean out unused variables
Done: how to send UDP packets from the UPP AP to my client laptop.  I now have it's IP address! /home/parkview/Documents/Projects/USB_Protected_Power_Supply/Code/PDM-UDP_Example/ESP32-PDM_Microphone-Breakout/Code/ossg_firmware_mcu/src/
  Another example:  https://www.engineersgarage.com/client-server-communication-over-udp-protocol-iot-part-31/  Arduino UDP to Python!
* add AJAX to the webpage to display live-ish current/voltages: https://circuits4you.com/2018/11/20/web-server-on-esp32-how-to-update-and-display-sensor-values/
* show current client connection IP on webpage
Done: get the board time setup via the config webpage.  Doing it via a code upload is painful
Done: change the SD card CSV filename from static to a date-time based name.
Done: get measured currents working and sus out quiesent current
Done: get PCF85063ATL RTC being written to and time read out
Done: add battery clip to back of PCB for battery
Done: check code for two i2C variable resistors
Done: then time for a new PCB Version: V0.6
Done: convert code to use either the SSD1306 (128x64) or SSD1327 (128x128) OLED Displays
Done: get Rotary Encoder going with some basic menus
Done:  1) AD5247BKSZ100-RL7 = measure resistances/voltages changes etc
Done: check D1 RGB LED - rewire
Done: current config is written to RTC Flash, so the board is auto configured upon startup!  Very nice feature!
*/

#define wifi                      // if present, will compile in WiFi libraries and code. This will slow down dev work due to time needed to compile. Can be remmed out if needed
#include <Arduino.h>              // Arduino code below
#include "FastLED.h"              // runs the WS2812 RGB LEDs
#include "AiEsp32RotaryEncoder.h" // reads the Rotary Encoder
#include <PCF85063TP.h>           // Seeed's RTC Library: https://github.com/Seeed-Studio/Grove_High_Precision_RTC_PCF85063TP
#include "SPI.h"                  // ESP32 SPI header
#include "SD.h"                   // used to access the SD card
#include "FS.h"                   // used with accessing SD card files
#include "PCAL9535A.h"            // add in header for the PCA9535 i2C GPIO port expander IC
#include <Wire.h>                 // i2C bus access
#include <Arduino_GFX_Library.h>  // used for LCD display
#include <Preferences.h>          // stores up to 512B of data perminantly in NVS Flash
#ifdef wifi
#include "WiFi.h"              // via the built in ESP Arduino Library?
#include <WiFiClient.h>        // via the built in ESP Arduino Library?
#include <WiFiAP.h>            // via the built in ESP Arduino Library?
#include <esp_wifi.h>          // via the built in ESP Arduino Library?
#include <AsyncTCP.h>          // via the built in ESP Arduino Library?
#include <ESPAsyncWebServer.h> // via the ottowinter/ESPAsyncWebServer-esphome@^1.3.0 library
#endif

#define softwareVersion "v0.8.3" // code version - major number should match the board version
#define NUM_RGBleds 20           // number of WS2812-2020 RGB LEDs on the board
#define LED_TYPE WS2812B         // version of the LED
#define COLOR_ORDER GRB          // set the correct color sequence
#define BRIGHTNESS 15            // how bright the RGB LEDs should be - not bright
#define RGB_PIN 17               // RGB LED GPIO pin
#define SDA 21                   // i2C SDA GPIO pin
#define SCL 47                   // i2C SCL clock GPIO pin
// PCA9535 Ports 0 to 15
#define usb1_EN 0           // turn on USB Micro input
#define usb2_EN 1           // turn on USB C input
#define calib_EN 2          // User SW3 input
#define cal1_EN 3           // turn on calibration 1: 500mA 10 Ohm load
#define cal2_EN 4           // turn on calibration 2: 100mA 50 Ohm load
#define cal3_EN 5           // turn on calibration 3: 10mA  500 Ohm load
#define cal4_EN 6           // turn on calibration 4: 1mA   5K Ohm load
#define cal5_EN 7           // turn on calibration 5: 100uA 50K Ohm load
#define cal6_EN 8           // turn on calibration 6: 10uA  500k Ohm load
#define bypass_EN 9         // User SW4 input
#define vAdjustBypass_EN 10 // bypass only the voltage adjustment
#define vadj_EN 11          // turn on voltage adjustment path
#define usb_Out_EN 12       // turn on USB output connector
#define sense_EN 13         // User SW2 input
#define bypassAll_EN 14     // turn on the bypass voltage and current adjustment paths
#define PH_IN_EN 15         // turn on the PH input
#define curr_EN 38          // turns on the Current Limiter MIC2097 IC
#define PCA9535_INT_Pin 48  // PCA9535 GPIO Expander Interrupt pin, needs a pullup turned on!!
#define bootMode_Pin 0      // boot/Mode switch Interrupt pin
// SPI ADC pins
#define HSPI_SCLK 13 // SPI Clock
#define HSPI_MISO 16 // SPI MISO
#define HSPI_MOSI 14 // SPI MOSI
#define HSPI_CS 15   // SPI ADC Chip Select
#define HSPI_DC 2    // CS for LCD
#define HSPI_CS1 1   //  OLED CS pin
#define HSPI_RST -1  // OLED RST pin (not used)
// SD Card GPIO:
#define SD_CS 41
//  other GPIO
#define ledRunPin 46     // program running LED
#define senseLoOutPin 17 // 0.4 Ohm sense resistor
#define senseHiOutPin 16 // 0.1 Ohm
#define usbOutPin 18
#define voltIN_EN 18
#define voltOUT_EN 8
#define calibration100mAPin 17
#define calibration1APin 5
#define calibration500mAPin 27
#define pot5kAddress 0x2f   // MCP4019-5KLT i2C digital potentiometer address
#define pot100kAddress 0x2e // AD5247BKSZ100 100K ohm i2C digital potentiometer address
#define ROTARY_ENCODER_BUTTON_PIN 7
#define ROTARY_ENCODER_A_PIN 5
#define ROTARY_ENCODER_B_PIN 4
#define ROTARY_ENCODER_STEPS 4   // 1, 2, 4 steps per rotary step bump
#define ROTARY_ENCODER_VCC_PIN 1 // Encoder is connected direct to VCC.  CHANGED to 1 and the error when away!
#define sense1_EN 39             // turns on 20 ohm current sense path: 10uA
#define sense2_EN 10             // turns on 2 ohm current sense path: 100uA
#define sense3_EN 11             // turns on 0.2 ohm current sense path: 500mA?
#define sense4_EN 12             // turns on 0.05 ohm current sens path: 2A?
// ADC Ports 0 to 7
#define sense1_VADC_CHNL 0
#define sense2_VADC_CHNL 1
#define sense3_VADC_CHNL 2
#define sense4_VADC_CHNL 3
#define usbIN_vADC_CHNL 4
#define VADJ_vADC_CHNL 5
#define calib_vADC_CHNL 6
#define usbOut_vADC_CHNL 7
#define TEXT_ALIGN_LEFT 0
#define TEXT_ALIGN_RIGHT 2
#define flipMode 0 // flipMode = 0 disable; 1=enabled
                   // #define SSD1327 // i2C OLED monocolor 128x128
                   // #define SSD1306 // i2C OLED monocolor 128x64
#define SSD1351    // SPI OLED color Screen 128x128

#ifdef SSD1351
// SSD1351 SPI 128x128 OLED screen init
Arduino_DataBus *bus = new Arduino_HWSPI(HSPI_DC /* DC */, HSPI_CS1 /* CS */, HSPI_SCLK /* SCK */, HSPI_MOSI /* MOSI */, HSPI_MISO /* MISO */, &SPI, true);
Arduino_GFX *gfx = new Arduino_SSD1351(bus, HSPI_RST /* RST */, 0 /* rotation */);
const uint8_t DISPLAY_WIDTH = 128;  // OLED display pixel width
const uint8_t DISPLAY_HEIGHT = 128; // OLED display pixel hight
#endif

#ifdef ST7789
// setup for 240x240 color LCD

#endif
#ifdef SSD1327
// U8G2_SSD1327_EA_W128128_F_HW_I2C u8g2(U8G2_R0, /* reset=*/U8X8_PIN_NONE /* CLK*/ /*SDA */); /* Uno: A4=SDA, A5=SCL, add "u8g2.setBusClock(400000);" */
U8G2_SSD1327_MIDAS_128X128_F_HW_I2C u8g2(U8G2_R0, /* reset=*/U8X8_PIN_NONE, SCL, SDA);
const uint8_t DISPLAY_WIDTH = 128;  // OLED display pixel width
const uint8_t DISPLAY_HEIGHT = 128; // OLED display pixel hight
#endif
#ifdef SSD1306
U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/U8X8_PIN_NONE, /* clock=*/22, /* data=*/21); // ESP32 Thing, HW I2C with pin remapping
const uint8_t DISPLAY_WIDTH = 128;                                                                        // OLED display pixel width
const uint8_t DISPLAY_HEIGHT = 64;                                                                        // OLED display pixel hight
#endif
CRGB leds[NUM_RGBleds];
AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, ROTARY_ENCODER_VCC_PIN, ROTARY_ENCODER_STEPS);
PCD85063TP RTclock; // define a object of PCD85063TP class
PCAL9535A::PCAL9535A<TwoWire> gpio(Wire);

const uint8_t BUZZPWM_CHANNEL = 0;    // ESP32 has 16 channels which can generate 16 independent waveforms
const int BUZZPWM_FREQ = 4000;        // Recall that Arduino Uno is ~490 Hz. Official ESP32 example uses 5,000Hz
const uint8_t BUZZPWM_RESOLUTION = 8; // We'll use same resolution as Uno (8 bits, 0-255) but ESP32 can go up to
// const int BUZZMAX_DUTY_CYCLE = (int)(pow(2, BUZZPWM_RESOLUTION) - 1);
const uint8_t buzzerPin = 6;            // Version 0.6  and above PCB's
const uint8_t LEDPWRPWM_CHANNEL = 2;    // ESP32 has 16 channels which can generate 16 independent waveforms
const int LEDPWRPWM_FREQ = 5000;        // Recall that Arduino Uno is ~490 Hz. Official ESP32 example uses 5,000Hz
const uint8_t LEDPWRPWM_RESOLUTION = 8; // We'll use same resolution as Uno (8 bits, 0-255) but ESP32 can go up to
// const int LEDPWRMAX_DUTY_CYCLE = (int)(pow(2, LEDPWRPWM_RESOLUTION) - 1);
const int DELAY_MS = 4;                                                                  // delay between fade increments
int _ledFadeStep = 5;                                                                    // amount to fade per loop
bool circleValues = true;                                                                // circle around rotary encoder or not
uint8_t menuState = 1;                                                                   // entry state of the menuState machine
u8_t calibrationMode = 0;                                                                // tracks current calibration/Ouput state
char *inputMode[4] = {"None", "USBMicro", "USB-C", "JST"};                               // modes: 0=nothing; 1=USB-Mico; 2=USB-C; 3=JST
char *outputMode[8] = {"None", "500mA", "100mA", "10mA", "1mA", "100uA", "10uA", "USB"}; // Cal0 = None; Cal1 = 500mA; Cal2 = 100mA; Cal3 = 10mA; Cal4 = 1mA; Cal5 = 100uA; Cal6 = 10uA; Cal7 = USB Out
char *samplesPerSec[6] = {"10000", "5000", "2000", "1000", "500", "BTN"};                // length of recording is dependant on Samples Per Second
char *sensePath[5] = {"None", "20", "2", ".2", ".05"};                                   // all possible current sense paths
uint8_t senseRes = 3;                                                                    // initial sense resistor index value
static const int spiClk = 20000000;                                                      // Min is 8MHz, Max is 16MHz
const float vRef = 4.097;                                                                // ADC128 Reference Voltage.  This needs to be changed if you modify the PCB design (R15/R14 ratio)
const float vADC = vRef / 4096;                                                          // minimal ADC unit
bool buttonX = false;                                                                    // tracks if SW1-SW3 button switch has been pressed
const float Voffset = 0.0;                                                               // not used anymore - v0.7/v0.8+
u8_t mode = 0;                                                                           // tracks the current board/screen mode  :  runtime | Setup | other?
char *modeText[2] = {"Run", "Setup"};                                                    // lists the human readable mode options
bool buttonM = false;                                                                    // tracks if Boot/Mode button switch (top RH corner) has been pressed
bool wifiMode = false;                                                                   // tracks wifi on/off mode/status
bool autoInputMode = false;                                                              // tracks if auto power input is on/off
bool rgbTest = false;                                                                    // tracks if the RGB test is running or not (on/off)
const bool setTimeEnable = false;                                                        // if set to true, stores the current time into the RTC IC.  Could be a webpage config thing!
File myFile;                                                                             // file descriptor for the SD card
SPIClass *hspi = NULL;                                                                   // ESP32-S3 SPI Class
Preferences preferences;                                                                 // class used to read/write to perminent RTC Flash memory for storing board config
const char *tcpAddress = "192.168.4.2";                                                  // Python host UDP receiver
const u16_t tcpPort = 8111;                                                              // UDP host port
#define LEAP_YEAR(Y) ((Y > 0) && !(Y % 4) && ((Y % 100) || !(Y % 400)))

#ifdef wifi
// String new_Str.toCharArray(inputName, 5);
// String inputName[] = "Paul";
char *clientIPAddress = "192.168.4.2";
char *PARAM_INPUT_1 = "datetime";
char *PARAM_INPUT_2 = "sps";
char *PARAM_INPUT_3 = "ipaddress";
bool wifiRunOnce = false;
WiFiUDP udp;
/*
const char* PARAM_INPUT_1 = "input1";
const char* PARAM_INPUT_2 = "input2";
const char* PARAM_INPUT_3 = "input3";
*/

// pre-list some functions ahead of use
float readCurrentA(void);
float readVoltageIn(SPIClass *spi, uint8_t);
void displayText(int, char *);
float readADC(SPIClass *spi, uint8_t);
float readSenseCurrent(SPIClass *spi, uint8_t);

// HTML web page to handle 3 input fields (input1, input2, input3)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>USB Power Profiler v0.8</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
    <style>
    html {
     font-family: Arial;
     display: inline-block;
     margin: 0px auto;
     text-align: left;
    }
    h2 { font-size: 2.0rem; color: #101090}
    p { font-size: 2.0rem; }
    .units { font-size: 1.5rem; vertical-align:middle;}
    .vc-labels{
      font-size: 1.5rem;
      vertical-align:middle;
      padding-bottom: 15px;
    }
    .value {font-size: 1.6rem;
      color: #05058a; 
      font-weight:600}
    .examples {font-size: 0.9rem;
      color: #903030;
    }
  </style>
  </head><body>
  <h2>USB Power Profiler v0.8</h2>
  <form action="/get">
  <span class="examples">Format: YYYY-MM-DD hh:mm:ss<BR></span>
    DateTime: <input type="text" name="datetime">
    <input type="submit" value="Submit">
  </form><br>
  <form action="/get">
  <span class="examples">Examples: 1000, 5000<BR></span>
    Samples Per Second: <input type="text" name="sps">
    <input type="submit" value="Submit">
  </form><br>
  <form action="/get">
  <span class="examples">Example: 192.168.4.2<BR></span>
    Client IP Add: <input type="text" name="ipaddress">
    <input type="submit" value="Submit">
  </form><br>
  <p>
    <i class="fas fa-voltage-half" style="color:#059e8a;"></i> 
    <span class="vc-labels">Voltage:</span> 
    <span class="value"; id="voltage">%VOLTAGE%</span>
    <sup class="units">V</sup><br>

    <i class="fas fa-tint" style="color:#00add6;"></i> 
    <span class="vc-labels">Current:</span>
    <span class="value"; id="current">%CURRENT%</span>
    <sup class="units">mA</sup>
  </p>
</body>
<script>
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("voltage").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/voltage", true);
  xhttp.send();
}, 10000 ) ;

setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("current").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/current", true);
  xhttp.send();
}, 10000 ) ;
</script>
</html>)rawliteral";

String processor(const String &var)
{
  // this is called as a suproutine from the index.html page via Setup() and pushes values to the webpage
  if (var == "VOLTAGE")
  {
    String dispVoltage = String(readVoltageIn(hspi, 7)); // Vin = 6; Vout = 7
    Serial.print("dispVolts: ");
    Serial.println(dispVoltage);
    return dispVoltage;
  }
  else if (var == "CURRENT")
  {
    return String(readSenseCurrent(hspi, senseRes)); // go read the current sense current
  }
  return String();
}
#endif

struct Button
{
  const uint8_t PIN;
  bool pressed;
};

typedef struct
{
  const uint8_t LED;  // WS2812 LED number ascosiated with the path
  char senseResistor; // resistor value for displaying
  uint8_t senseGPIO;  // GPIO to turn on the path
  uint8_t senseADC;   // ADC Port number that we need to read for this path
  float senseMaths;   // maybe holds the divisor needed for the current calculation?
} sense;

// TODO: the next 3 lines need re-factoring, as they no longer can call an ESP32 interupts.  Will need to be called via the Expander Interupt
Button button1 = {bypass_EN, false};
Button button2 = {sense_EN, false};
Button button3 = {calib_EN, false};
// Button buttonM = {calib_EN, false};

#ifdef wifi
// Need to add a MAC like rnd number after SSID name
const char *ssid = "USBPowerProfiler";
const char *password = "";
// const char* password = "12345678";
int wifi_signal = 0;

// Set web server port number to 80
AsyncWebServer server(80);
// WiFiServer server(80);
bool wifiEn = false;

// Variable to store the HTTP request
String header;

void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "Not found");
}

void StopWiFi()
{
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
}

uint8_t StartWiFi()
{
  // TODO: show IP address for first few goes or when in Portrate mode.  Do DNS capture/redirect
  Serial.print("\r\nConnecting to: ");
  Serial.println(String(ssid));
  IPAddress dns(8, 8, 8, 8); // Google DNS
  WiFi.disconnect();
  WiFi.mode(WIFI_STA); // switch off AP
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  WiFi.begin(ssid, password);
  unsigned long start = millis();
  uint8_t connectionStatus;
  bool AttemptConnection = true;
  while (AttemptConnection)
  {
    connectionStatus = WiFi.status();
    if (millis() > start + 15000)
    { // Wait 15-secs maximum
      AttemptConnection = false;
    }
    if (connectionStatus == WL_CONNECTED || connectionStatus == WL_CONNECT_FAILED)
    {
      AttemptConnection = false;
    }
    delay(50);
  }
  if (connectionStatus == WL_CONNECTED)
  {
    wifi_signal = WiFi.RSSI(); // Get Wifi Signal strength now, because the WiFi will be turned off to save power!
    Serial.println("WiFi connected at: " + WiFi.localIP().toString());
  }
  else
    Serial.println("WiFi connection *** FAILED ***");
  return connectionStatus;
}
#endif

u8_t dayOfWeek(uint16_t year, uint8_t month, uint8_t day)
{
  // This works, Sunday = 0, Monday = 1... Saturday = 6
  Serial.println(year);
  Serial.println(month);
  Serial.println(day);
  uint16_t months[] = {
      0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365}; // days until 1st of month

  uint32_t days = year * 365; // days until year
  for (uint16_t i = 4; i < year; i += 4)
    if (LEAP_YEAR(i))
      days++; // adjust leap years, test only multiple of 4 of course

  days += months[month - 1] + day; // add the days of this year
  if ((month > 2) && LEAP_YEAR(year))
    days++; // adjust 1 if this year is a leap year, but only after febr
  Serial.print("Days: ");
  Serial.println(days);
  Serial.print("Days of Week: ");
  Serial.println(days % 7);
  u8_t goodDoW = (days % 7) - 1; // remove all multiples of 7 and offset by one to match my RTC Day of Week
  return goodDoW;
}

void IRAM_ATTR readEncoderISR()
{
  rotaryEncoder.readEncoder_ISR();
}

void IRAM_ATTR PCA9535()
{
  // one of the board's 3 tactile buttons have been pressed
  buttonX = true;
}

void IRAM_ATTR modeSW()
{
  // the Boot/Mode button has been pressed
  buttonM = true;
}

void displayText(uint8_t x, uint8_t y, uint16_t color, const char *text)
{
// display simple text at a X/Y (0-128) location on the screen in a set RGB colored font.
// NOTE: this could be expanded out to include a background color as well?
// TODO: check for 0-128 boundries
// TODO: fix the string references?  What else can I pass through?
#ifdef SSD1351
  gfx->setCursor(x, y);
  gfx->setTextColor(color);
  gfx->println(text);
#endif
}

void displayInt(uint8_t x, uint8_t y, uint16_t color, u16_t text)
{
// display simple text at a X/Y (0-128) location on the screen in a set RGB colored font.
// NOTE: this could be expanded out to include a background color as well?
// TODO: check for 0-128 boundries
// TODO: fix the string references?  What else can I pass through?
#ifdef SSD1351
  gfx->setCursor(x, y);
  gfx->setTextColor(color);
  gfx->print(text);
#endif
}

void displayFloat(uint8_t x, uint8_t y, uint16_t color, float text)
{
// display simple text at a X/Y (0-128) location on the screen in a set RGB colored font.
// NOTE: this could be expanded out to include a background color as well?
// TODO: check for 0-128 boundries
// TODO: fix the string references?  What else can I pass through?
#ifdef SSD1351
  gfx->setCursor(x, y);
  gfx->setTextColor(color);
  gfx->printf("%3.2f;\n", &text);
#endif
}

void OLED_Header()
{
  char msg[14];
  float USBVinput = 0.0;
  float USBAinput = 0.0;
#ifdef SSD1351
  gfx->fillScreen(BLACK); // is there a better way to clear the screen?
  gfx->setCursor(1, 0);
  gfx->setTextColor(GREEN);
  gfx->println("UPP:");
  gfx->setTextColor(CYAN);
  USBVinput = readVoltageIn(hspi, 7);                     // Vin = 6; Vout = 7
  USBAinput = readSenseCurrent(hspi, senseRes) - Voffset; // was senseRes + 1
  gfx->setCursor(25, 0);
  gfx->printf("%.2fV", USBVinput);
  gfx->setCursor(69, 0);
  gfx->printf("%.4fmA", USBAinput);
  gfx->drawLine(1, 8, DISPLAY_WIDTH, 8, RED);
#endif
#ifdef SSD1327
  u8g2.clear();
  u8g2.sendBuffer();
  u8g2.setDrawColor(2); // 0=dark; 1=on; 2=xor
  u8g2.setFontPosBottom();
  u8g2.drawStr(1, 10, "UP"); // only the lower part of the text can be seen
  u8g2.drawHLine(1, 12, DISPLAY_WIDTH);
  USBVinput = readVoltageIn(hspi, 4);
  float USBAinput = readSenseCurrent(hspi, senseRes + 1);
  u8g2.setCursor(28, 10); // set up for voltage
  u8g2.printf("%.2fV", USBVinput);
  u8g2.setCursor(69, 10); // set up for voltage
  u8g2.printf("%.3fmA", USBAinput);
#endif
#ifdef SSD1351
  Serial.print(">Current:"); // formatted for TelePlot graphing
  Serial.println(USBAinput, 4);
// Serial.printf("0%4f",USBAinput );
#endif
#ifdef SSD1327
  Serial.print(USBAinput);
#endif
  // Serial.println("mA");
#ifdef SSD1327
  // u8g2.updateDisplay();  // don't use this.  Use sendBuffer() instead
  u8g2.sendBuffer();
#endif
}

float readCurrentA(uint8_t chnl)
{
  // convert Sense channel to ADC channel
  chnl -= 1;
  readADC(hspi, chnl); // change to read current channel
  float vChnl = readADC(hspi, chnl);
  return vChnl;
}

float readVoltageIn(SPIClass *spi, uint8_t channel)
{
  // Read voltage from input/output USB voltage divider:  Input=6; Output=7
  // channel will be 4 to 7 (ADC channel numbers)
  float vUSBin = 0.0;
  float vTemp1 = 0.0;
  float vTemp2 = 0.0;
  uint16_t hiByte = 0;
  uint8_t chann = channel - 3;
  const uint16_t bufferSize = 16; // take an average of this number of readings
  uint16_t chan = (channel & 0x7) << 11;
  if (channel == 6)
  {
    // turn on input voltage divider
    digitalWrite(18, HIGH); // enable Vout=8; enable Vin=18
  }
  else if (channel == 7)
  {
    // turn on output voltage divider
    digitalWrite(8, HIGH); // enable Vout=8; enable Vin=18
  }                        // otherwise error!
  // NOTE: don't put a long delay here, as it stuffs up the display blanking making it look bad. 200-300uSec  uSec should be ok
  delayMicroseconds(300);         // let the voltage settle (doesn't seem to matter though).  Be good to scope this one day!
  digitalWrite(HSPI_CS, LOW);     // pull ss/cs low to turn on ADC128 IC and to initiate data transfer
  hiByte = spi->transfer16(chan); // send it the channel number once as a dummy run to get ADC moved to new channel
  delayMicroseconds(5);           // wait for channel to change.  Estimated delay.  Needs to be checked with a scope.
  for (uint16_t i = 0; i <= bufferSize; i++)
  {
    vTemp1 += spi->transfer16(chan); // we send it the channel number shifted into the MSB and get back the 12bit reading
  }
  digitalWrite(HSPI_CS, HIGH); // pull ss/cs high to signify end of data transfer. only need to do this once though.  Fix the code.
  vTemp2 = (vTemp1 / bufferSize) * vADC;
  if (channel == 6)
  {
    // turn on input voltage divider
    digitalWrite(18, LOW); // enable Vout=8; enable Vin=18
    vUSBin = vTemp2 * 1.885;  // 0.005V out!  Close enough for 5V work
  }
  else if (channel == 7)
  {
    // turn on output voltage divider
    digitalWrite(8, LOW); // enable Vout=8; enable Vin=18
    vUSBin = vTemp2 * 1.910; // this seems very accurate for UPP v0.8 PCB.  R73/R74 are both 100K 0.1%. So need to times the reading by two. + the buffer value
  }                       // otherwise error!
  return vUSBin;
}

float readSenseCurrent(SPIClass *spi, uint8_t channel)
{
  // Read voltage from current sense resistors, via INA180
  float vSense = 0.0;
  uint16_t hiByte = 0;
  float current = 0.0;
  const uint8_t ina180Amp = 50;
  if ((channel > 0) && (channel < 5))
  {
    const uint16_t bufferSize = 16;              // take an average of this number of readings
    uint16_t chan = ((channel - 1) & 0x7) << 11; // convert sense channel into correct placement for ADC IC register
    digitalWrite(HSPI_CS, LOW);                  // pull ss/cs low to turn on ADC128 IC and to initiate data transfer
    hiByte = spi->transfer16(chan);              // send it the channel number once as a dummy run to get ADC moved to new channel
    for (uint16_t i = 0; i < bufferSize; i++)
    {
      vSense += spi->transfer16(chan); // we send it the channel number shifted into the MSB and get back the 12bit reading
    }
    digitalWrite(HSPI_CS, HIGH); // pull ss/cs high to signify end of data transfer. only need to do this once though.  Fix the code.
    // do the maths for current calculation
    /*
      Serial.println(channel);
      Serial.println(vSense);
      Serial.println(vSense/bufferSize);
      Serial.println((vSense/bufferSize)/ina180Amp);
      */
    switch (channel)
    {
    case 1:
      // can just bit shift vSense << 16 instead of the divide by buffer size
      current = (vSense / bufferSize) / ina180Amp / 20.4; // 100/20 ohm resistor
      break;
    case 2:
      current = (vSense / bufferSize) / ina180Amp / 2; // 2 ohm resistor
      break;
    case 3:
      current = (vSense / bufferSize) / ina180Amp / .2; // 0.2 ohm resistor
      break;
    case 4:
      current = (vSense / bufferSize) / ina180Amp / .05; // 0.05 ohm resistor
      break;
    default:
      Serial.print(channel);
      Serial.println(" ERROR: readSenseCurrent - should never get here");
      break;
    }
    // Serial.println(vSense);
    // Serial.println(current);
    return (current * vADC * 1000); // returns milliAmps
  }
}

void printTime()
{
  RTclock.getTime();
  Serial.print(RTclock.year + 2000, DEC);
  Serial.print("-");
  Serial.print(RTclock.month, DEC);
  Serial.print("-");
  Serial.print(RTclock.dayOfMonth, DEC);
  Serial.print(" ");
  Serial.print(RTclock.hour, DEC);
  Serial.print(":");
  Serial.print(RTclock.minute, DEC);
  Serial.print(":");
  Serial.print(RTclock.second, DEC);
  Serial.print(" ");
  switch (RTclock.dayOfWeek)
  { // Friendly printout the weekday
  case MON:
    Serial.print("Monday");
    break;
  case TUE:
    Serial.print("Tuesday");
    break;
  case WED:
    Serial.print("Wednsday");
    break;
  case THU:
    Serial.print("Thursday");
    break;
  case FRI:
    Serial.print("Friday");
    break;
  case SAT:
    Serial.print("Saturday");
    break;
  case SUN:
    Serial.print("Sunday");
    break;
  }
  Serial.println(" ");
}

void rgbTestLED()
{
  // turn on all RGB LEDs to make sure they all are working
  const uint16_t RGBLEDDelay = 2500;
  Serial.println("Flash RGB LEDs");
  leds[0] = CRGB::Red;
  leds[1] = CRGB::Green;
  leds[2] = CRGB::Blue;
  leds[3] = CRGB::Yellow;
  leds[4] = CRGB::Red;
  leds[5] = CRGB::Orange;
  leds[6] = CRGB::Brown;
  leds[7] = CRGB::Purple;
  FastLED.show();
  delay(RGBLEDDelay);
  // Now turn the LED off, then pause
  leds[0] = CRGB::Black;
  leds[1] = CRGB::Black;
  leds[2] = CRGB::Black;
  leds[3] = CRGB::Black;
  leds[4] = CRGB::Black;
  leds[5] = CRGB::Black;
  leds[6] = CRGB::Black;
  leds[7] = CRGB::Black;
  FastLED.show();
  Serial.println("RGB LEDs Off!");
}

void startRecording(uint8_t speed)
{
  // start the sensor recording.  How to determine which current sense resistor we are using?
  // lets start with just the 100m Ohm + INA219A

  // SPS Speed Options:  0=1000; 1=500; 2=100; 3=50; 4=10
  //  set delay, get RTC, Loop: get INA219 data, delay()582Grd, Bytes INA219A voltage, Bytes of INA219 current,
  float buffer[10000];
}

void enableVoltagePath(uint8_t outDevice)
// function deals with all voltage paths
{
  switch (outDevice)
  {
  case 0:
    // turn off and isolate all voltage paths
    Serial.println("Turn off V adjust paths");
    gpio.digitalWrite(vAdjustBypass_EN, LOW);
    gpio.digitalWrite(bypassAll_EN, LOW);
    gpio.digitalWrite(vadj_EN, LOW);
    leds[4] = CRGB::Red;
    leds[5] = CRGB::Red;
    leds[6] = CRGB::Red;
    leds[7] = CRGB::Red;
    leds[8] = CRGB::Red;
    break;
  case 1:
    // turn on voltage path
    Serial.println("Turn on V adjust path");
    gpio.digitalWrite(vAdjustBypass_EN, LOW);
    gpio.digitalWrite(bypassAll_EN, LOW);
    gpio.digitalWrite(vadj_EN, HIGH);
    leds[4] = CRGB::Red;
    leds[5] = CRGB::Green;
    leds[6] = CRGB::Green;
    leds[7] = CRGB::Red;
    leds[8] = CRGB::Green;
    break;
  case 2:
    // Bypass voltage path
    Serial.println("Bypass Voltage adjust path");
    gpio.digitalWrite(bypassAll_EN, LOW);
    gpio.digitalWrite(vadj_EN, LOW);
    gpio.digitalWrite(vAdjustBypass_EN, HIGH);
    leds[4] = CRGB::Red;
    leds[5] = CRGB::Red;
    leds[6] = CRGB::Red;
    leds[7] = CRGB::Green;
    leds[8] = CRGB::Green;
    break;
  case 3:
    // Bypass all voltage and current paths
    Serial.println("Bypass all voltage and current adjust paths");
    gpio.digitalWrite(vAdjustBypass_EN, LOW);
    gpio.digitalWrite(vadj_EN, LOW);
    gpio.digitalWrite(bypassAll_EN, HIGH);
    leds[4] = CRGB::Green;
    leds[5] = CRGB::Red;
    leds[6] = CRGB::Red;
    leds[7] = CRGB::Red;
    leds[8] = CRGB::Red;
    break;
  default:
    Serial.print(outDevice);
    Serial.println(" ERROR: Voltage path");
    break;
  }
}

void enableCurrentSense(uint8_t outDevice)
// function deals with all current sense paths
{
  switch (outDevice)
  {
  case 0:
    // turn off all current sensing paths
    Serial.println("Turn off all current sense paths");
    digitalWrite(sense1_EN, LOW);
    digitalWrite(sense2_EN, LOW);
    digitalWrite(sense3_EN, LOW);
    digitalWrite(sense4_EN, LOW);
    leds[9] = CRGB::Red;
    leds[10] = CRGB::Red;
    leds[11] = CRGB::Red;
    leds[12] = CRGB::Red;
    break;
  case 1:
    // turn on 20 ohm current sense path
    Serial.println("Turn on 20 ohm current sense path");
    digitalWrite(sense1_EN, HIGH);
    digitalWrite(sense2_EN, LOW);
    digitalWrite(sense3_EN, LOW);
    digitalWrite(sense4_EN, LOW);
    leds[9] = CRGB::Green;
    leds[10] = CRGB::Red;
    leds[11] = CRGB::Red;
    leds[12] = CRGB::Red;
    break;
  case 2:
    // turn on 2 ohm current sense path
    Serial.println("Turn on 2 ohm current sense path");
    digitalWrite(sense1_EN, LOW);
    digitalWrite(sense2_EN, HIGH);
    digitalWrite(sense3_EN, LOW);
    digitalWrite(sense4_EN, LOW);
    leds[9] = CRGB::Red;
    leds[10] = CRGB::Green;
    leds[11] = CRGB::Red;
    leds[12] = CRGB::Red;
    break;
  case 3:
    // turn on 0.2 ohm current sense path
    Serial.println("Turn on 0.2 ohm current sense path");
    digitalWrite(sense1_EN, LOW);
    digitalWrite(sense2_EN, LOW);
    digitalWrite(sense3_EN, HIGH);
    digitalWrite(sense4_EN, LOW);
    leds[9] = CRGB::Red;
    leds[10] = CRGB::Red;
    leds[11] = CRGB::Green;
    leds[12] = CRGB::Red;
    break;
  case 4:
    // turn on 0.05 ohm current sense path
    Serial.println("Turn on 0.05 ohm current sense path");
    digitalWrite(sense1_EN, LOW);
    digitalWrite(sense2_EN, LOW);
    digitalWrite(sense3_EN, LOW);
    digitalWrite(sense4_EN, HIGH);
    leds[9] = CRGB::Red;
    leds[10] = CRGB::Red;
    leds[11] = CRGB::Red;
    leds[12] = CRGB::Green;
    break;
  default:
    Serial.print(outDevice);
    Serial.println(" ERROR: Sense path");
    break;
  }
  FastLED.show();
  preferences.begin("upp", false); // open RTC Flash for writing
  preferences.putUInt("PSenseRes", outDevice);
  preferences.end();
}

void enableCalibration(uint8_t outDevice)
// function deals with all Output possibilities
{
  switch (outDevice)
  {
  case 0:
    // no output
    Serial.println("Turn off all Outputs");
    gpio.digitalWrite(cal1_EN, LOW);
    gpio.digitalWrite(cal2_EN, LOW);
    gpio.digitalWrite(cal3_EN, LOW);
    gpio.digitalWrite(cal4_EN, LOW);
    gpio.digitalWrite(cal5_EN, LOW);
    gpio.digitalWrite(cal6_EN, LOW);
    gpio.digitalWrite(usb_Out_EN, LOW);
    leds[14] = CRGB::Red; // Load 1 500mA
    leds[15] = CRGB::Red; // Load 2 100mA
    leds[16] = CRGB::Red; // Load 3 10mA
    leds[17] = CRGB::Red; // Load 4 1mA
    leds[18] = CRGB::Red; // Load 5 100uA
    leds[19] = CRGB::Red; // Load 6 10uA
    leds[13] = CRGB::Red; // USB Out
    FastLED.show();
    break;
  case 1:
    // 500mA Load
    Serial.println("Turn on 500mA calibration path");
    gpio.digitalWrite(cal2_EN, LOW);
    gpio.digitalWrite(cal3_EN, LOW);
    gpio.digitalWrite(cal4_EN, LOW);
    gpio.digitalWrite(cal5_EN, LOW);
    gpio.digitalWrite(cal6_EN, LOW);
    gpio.digitalWrite(usb_Out_EN, LOW);
    gpio.digitalWrite(cal1_EN, HIGH);
    leds[14] = CRGB::Green; // Load 1 500mA
    leds[15] = CRGB::Red;   // Load 2 100mA
    leds[16] = CRGB::Red;   // Load 3 10mA
    leds[17] = CRGB::Red;   // Load 4 1mA
    leds[18] = CRGB::Red;   // Load 5 100uA
    leds[19] = CRGB::Red;   // Load 6 10uA
    leds[13] = CRGB::Red;   // USB Out
    FastLED.show();
    break;
  case 2:
    // 100mA Load
    Serial.println("Turn on 100mA calibration path");
    gpio.digitalWrite(cal1_EN, LOW);
    gpio.digitalWrite(cal3_EN, LOW);
    gpio.digitalWrite(cal4_EN, LOW);
    gpio.digitalWrite(cal5_EN, LOW);
    gpio.digitalWrite(cal6_EN, LOW);
    gpio.digitalWrite(usb_Out_EN, LOW);
    gpio.digitalWrite(cal2_EN, HIGH);
    leds[14] = CRGB::Red;   // Load 1 500mA
    leds[15] = CRGB::Green; // Load 2 100mA
    leds[16] = CRGB::Red;   // Load 3 10mA
    leds[17] = CRGB::Red;   // Load 4 1mA
    leds[18] = CRGB::Red;   // Load 5 100uA
    leds[19] = CRGB::Red;   // Load 6 10uA
    leds[13] = CRGB::Red;   // USB Out
    FastLED.show();
    break;
  case 3:
    // 10mA Load
    Serial.println("Turn on 10mA calibration path");
    gpio.digitalWrite(cal1_EN, LOW);
    gpio.digitalWrite(cal2_EN, LOW);
    gpio.digitalWrite(cal4_EN, LOW);
    gpio.digitalWrite(cal5_EN, LOW);
    gpio.digitalWrite(cal6_EN, LOW);
    gpio.digitalWrite(usb_Out_EN, LOW);
    gpio.digitalWrite(cal3_EN, HIGH);
    leds[14] = CRGB::Red;   // Load 1 500mA
    leds[15] = CRGB::Red;   // Load 2 100mA
    leds[16] = CRGB::Green; // Load 3 10mA
    leds[17] = CRGB::Red;   // Load 4 1mA
    leds[18] = CRGB::Red;   // Load 5 100uA
    leds[19] = CRGB::Red;   // Load 6 10uA
    leds[13] = CRGB::Red;   // USB Out
    FastLED.show();
    break;
  case 4:
    // 1mA Load
    Serial.println("Turn on 1mA calibration path");
    gpio.digitalWrite(cal1_EN, LOW);
    gpio.digitalWrite(cal2_EN, LOW);
    gpio.digitalWrite(cal3_EN, LOW);
    gpio.digitalWrite(cal5_EN, LOW);
    gpio.digitalWrite(cal6_EN, LOW);
    gpio.digitalWrite(usb_Out_EN, LOW);
    gpio.digitalWrite(cal4_EN, HIGH);
    leds[14] = CRGB::Red;   // Load 1 500mA
    leds[15] = CRGB::Red;   // Load 2 100mA
    leds[16] = CRGB::Red;   // Load 3 10mA
    leds[17] = CRGB::Green; // Load 4 1mA
    leds[18] = CRGB::Red;   // Load 5 100uA
    leds[19] = CRGB::Red;   // Load 6 10uA
    leds[13] = CRGB::Red;   // USB Out
    FastLED.show();
    break;
  case 5:
    // 100uA Load
    Serial.println("Turn on 100uA calibration path");
    gpio.digitalWrite(cal1_EN, LOW);
    gpio.digitalWrite(cal2_EN, LOW);
    gpio.digitalWrite(cal3_EN, LOW);
    gpio.digitalWrite(cal4_EN, LOW);
    gpio.digitalWrite(cal6_EN, LOW);
    gpio.digitalWrite(usb_Out_EN, LOW);
    gpio.digitalWrite(cal5_EN, HIGH);
    leds[14] = CRGB::Red;   // Load 1 500mA
    leds[15] = CRGB::Red;   // Load 2 100mA
    leds[16] = CRGB::Red;   // Load 3 10mA
    leds[17] = CRGB::Red;   // Load 4 1mA
    leds[18] = CRGB::Green; // Load 5 100uA
    leds[19] = CRGB::Red;   // Load 6 10uA
    leds[13] = CRGB::Red;   // USB Out
    FastLED.show();
    break;
  case 6:
    // 10uA Load
    Serial.println("Turn on 10uA calibration path");
    gpio.digitalWrite(cal1_EN, LOW);
    gpio.digitalWrite(cal2_EN, LOW);
    gpio.digitalWrite(cal3_EN, LOW);
    gpio.digitalWrite(cal4_EN, LOW);
    gpio.digitalWrite(cal5_EN, LOW);
    gpio.digitalWrite(usb_Out_EN, LOW);
    gpio.digitalWrite(cal6_EN, HIGH);
    leds[14] = CRGB::Red;   // Load 1 500mA
    leds[15] = CRGB::Red;   // Load 2 100mA
    leds[16] = CRGB::Red;   // Load 3 10mA
    leds[17] = CRGB::Red;   // Load 4 1mA
    leds[18] = CRGB::Red;   // Load 5 100uA
    leds[19] = CRGB::Green; // Load 6 10uA
    leds[13] = CRGB::Red;   // USB Out
    FastLED.show();
    break;
  case 7:
    // USB Output
    Serial.println("Turn on USB Output connector");
    gpio.digitalWrite(cal1_EN, LOW);
    gpio.digitalWrite(cal2_EN, LOW);
    gpio.digitalWrite(cal3_EN, LOW);
    gpio.digitalWrite(cal4_EN, LOW);
    gpio.digitalWrite(cal5_EN, LOW);
    gpio.digitalWrite(cal6_EN, LOW);
    gpio.digitalWrite(usb_Out_EN, HIGH);
    leds[14] = CRGB::Red;   // Load 1 500mA
    leds[15] = CRGB::Red;   // Load 2 100mA
    leds[16] = CRGB::Red;   // Load 3 10mA
    leds[17] = CRGB::Red;   // Load 4 1mA
    leds[18] = CRGB::Red;   // Load 5 100uA
    leds[19] = CRGB::Red;   // Load 6 10uA
    leds[13] = CRGB::Green; // USB Out
    FastLED.show();
    break;
  default:
    Serial.print(outDevice);
    Serial.println(" ERROR: Calibration path");
    break;
  }
  preferences.begin("upp", false); // open RTC Flash for writing
  preferences.putUInt("PCalMode", outDevice);
  preferences.end();
}

void enableUSB(uint8_t usbPort)
{
  // turns on either 0: None, 1: USB Micro, 2: USB C or 3: PH connector input ports
  switch (usbPort)
  {
  case 0:
    // turn off all input Port
    Serial.println("Turn off all USB input Ports");
    gpio.digitalWrite(usb1_EN, LOW);
    gpio.digitalWrite(usb2_EN, LOW);
    gpio.digitalWrite(PH_IN_EN, LOW);
    leds[0] = CRGB::Red; // PH Connector
    leds[1] = CRGB::Red; // USB C
    leds[2] = CRGB::Red; // USB Micro
    break;
  case 1:
    // turn on USB Micro input Port
    Serial.println("Turn on USB Micro input Port");
    gpio.digitalWrite(usb2_EN, LOW);
    gpio.digitalWrite(usb1_EN, HIGH);
    gpio.digitalWrite(PH_IN_EN, LOW);
    leds[0] = CRGB::Red;   // PH Connector
    leds[1] = CRGB::Red;   // USB C
    leds[2] = CRGB::Green; // USB Micro
    break;
  case 2:
    // turn on USB C USB input Port
    Serial.println("Turn on USB C input Port");
    gpio.digitalWrite(usb1_EN, LOW);
    gpio.digitalWrite(usb2_EN, HIGH);
    gpio.digitalWrite(PH_IN_EN, LOW);
    leds[0] = CRGB::Red;   // PH Connector
    leds[1] = CRGB::Green; // USB C
    leds[2] = CRGB::Red;   // USB Micro
    break;
  case 3:
    // turn on JST-PH connector input Port
    Serial.println("Turn on JST-PH input Port");
    gpio.digitalWrite(usb1_EN, LOW);
    gpio.digitalWrite(usb2_EN, LOW);
    gpio.digitalWrite(PH_IN_EN, HIGH);
    leds[0] = CRGB::Green; // PH Connector
    leds[1] = CRGB::Red;   // USB C
    leds[2] = CRGB::Red;   // USB Micro
    break;
  default:
    Serial.print(usbPort);
    Serial.println("ERROR: USB input Port");
    break;
  }
  preferences.begin("upp", false); // open RTC Flash for writing
  preferences.putUInt("PInputMode", usbPort);
  preferences.end();
}

void testVoltages()
{
  // outputs i2C number and resultant output voltage reading
  //  can be used with serialPlot to generate a nice graph
  // turn on USB Micro path
  // Serial.println("Turn on USB Micro input");
  enableUSB(1);          // USB0 = no Ports; USB1 = USB Micro; USB2 = USB C
  enableVoltagePath(1);  // Vpath0 = non; Vpath1 = turn on Volt Adjust; Vpath2 = bypass Volt adjust; Vpath3 = bypass V & C paths
  enableCurrentSense(4); // sense0 = none; sense1 = 20 ohm; sense2 = 2 ohm; sense3 = 0.2 ohm; sense4 = 0.05 ohm
  enableCalibration(1);  // Cal0 = None; Cal1 = 500mA; Cal2 = 100mA; Cal3 = 10mA; Cal4 = 1mA; Cal5 = 100uA; Cal6 = 10uA;
  char msg[14];
  for (uint8_t i = 0; i <= 127; i++)
  {
    Wire.beginTransmission(pot100kAddress);
    Wire.write(i);
    Wire.endTransmission();
    delay(200);
    sprintf(msg, "%.3f", readVoltageIn(hspi, 4));
    Serial.print("Volt Adj: ");
    Serial.println(msg);
  }
}

void soundBuzzer(uint16_t buzzDelay)
{
  // sound the buzzer for specified length of time
  ledcWrite(BUZZPWM_CHANNEL, 20);
  delay(buzzDelay); // mSec delay
  ledcWrite(BUZZPWM_CHANNEL, 0);
}

void setTime()
{
  // use this to set a programmed datetime into PFC85063 RTC.  This works, but see the newer webpage datetime function
  RTclock.stopClock();
  RTclock.fillByYMD(2023, 9, 14); // May 27,2022
  RTclock.fillByHMS(15, 24, 00);  // 21:40 00"
  RTclock.fillDayOfWeek(THU);     // Friday
  RTclock.setTime();              // write time to the RTC chip
  RTclock.startClock();           // start the clock again
}

float readADC(SPIClass *spi, uint8_t channel)
{
  // reads the ADC128 8 channel ADC IC.  Pass in the channel number to read, and pass out the voltage being read.
  // very inefficient.  Should be using pointers with a buffer.
  uint16_t hiByte = 0;
  uint16_t chan = (channel & 0x7) << 11;
  digitalWrite(HSPI_CS, LOW);     // pull ss/cs low to turn on ADC128 IC and to initiate data transfer
  hiByte = spi->transfer16(chan); // we send it the channel number shifted into the MSB and get back the 12bit reading
  digitalWrite(HSPI_CS, HIGH);    // pull ss/cs high to signify end of data transfer
  return (hiByte * vADC);
}

void buffADC(SPIClass *spi, uint8_t channel, uint16_t buff[], uint16_t bufferSize)
// void buffADC(SPIClass *spi, uint8_t channel, uint16_t firetime, uint16_t buff[], uint8_t bufferSize)  // orig line
{
  // reads one of the ADC128 8 channel ADC Ports.  Pass in the channel number to read
  uint16_t hiByte = 0;
  uint16_t chan = (channel & 0x7) << 11;
  digitalWrite(HSPI_CS, LOW);     // pull ss/cs low to turn on ADC128 IC and to initiate data transfer
  hiByte = spi->transfer16(chan); // send it the channel number once as a dummy run to get ADC moved to new channel
  for (uint16_t i = 0; i < bufferSize; i++)
  {
    *(buff + i) = spi->transfer16(chan); // we send it the channel number shifted into the MSB and get back the 12bit reading
  }
  digitalWrite(HSPI_CS, HIGH); // pull ss/cs high to signify end of data transfer. only need to do this once though.  Fix the code.
}

void write_5K(uint8_t reg)
{
  Serial.println("send 5K pot data");
  Wire.beginTransmission(pot5kAddress);
  Wire.write(reg);
  Wire.endTransmission();
  preferences.begin("upp", false); // open RTC Flash for writing
  preferences.putUInt("Ppot5kValue", reg);
  preferences.end();
  Serial.println("5K pot data written!");
}

uint8_t read_5K(void)
{
  Serial.println("read 5K pot data");
  Wire.beginTransmission(pot5kAddress);
  Wire.endTransmission();
  Wire.requestFrom(pot5kAddress, 1);
  uint8_t reg = Wire.read();
  Wire.endTransmission();
  return reg;
}

void write_100K(uint8_t reg)
{
  Serial.println("send 100K pot data");
  Wire.beginTransmission(pot100kAddress);
  Wire.write(reg);
  Wire.endTransmission();
  preferences.begin("upp", false); // open RTC Flash for writing
  preferences.putUInt("Ppot100kValue", reg);
  preferences.end();
  Serial.println("100K pot data written!");
}

uint8_t read_100K(void)
{
  Serial.println("read 100K pot data");
  Wire.beginTransmission(pot100kAddress);
  Wire.endTransmission();
  Wire.requestFrom(pot100kAddress, 1);
  uint8_t reg = Wire.read();
  Wire.endTransmission();
  return reg;
}

void gpioTest(void)
{
  // this should send an approx 1Khz on/off square wave on GPIO 0, to R20 near USB Micro input
  for (uint8_t i = 0; i < 100000; i++)
  {
    gpio.digitalWrite(0, LOW);
    // delay(1);
    gpio.digitalWrite(0, HIGH);
    // delay(1);
  }
}

void i2C5vTest(void)
{
  //  this will test the 5V i2C line for the 5K and 100K pots.  Scope the lines out at the RN1 if required
  // check the 5K pot
  uint8_t readout = read_5K(); // initial power up value = 63
  Serial.print("5K pot data: ");
  Serial.println(readout);
  write_5K(23);
  readout = read_5K();
  Serial.print("5K pot data: ");
  Serial.println(readout);
  // check the 100K pot
  readout = read_100K(); // initial power up value = 64
  Serial.print("100K pot data: ");
  Serial.println(readout);
  write_100K(39);
  readout = read_100K();
  Serial.print("100K pot data: ");
  Serial.println(readout);
}

void checkSwitches(void)
{
  // check if any of the switches have been pressed (pressed SW = 0)
  if (buttonX)
  {
    // ok, so a button switch has been pressed - find out which one of the 3 has been pressed
    Serial.print("Interrupt State: ");
    Serial.println(buttonX);
    buttonX = false;
    uint16_t state = gpio.readGPIO16();
    Serial.println(" ");
    Serial.print("Button State: ");
    Serial.println(state, BIN);
    Serial.println(" ");
    if (bitRead(state, 9))
    {
      button1.pressed = false;
    }
    else
    {
      button1.pressed = true;
    }
    if (bitRead(state, 13))
    {
      button2.pressed = false;
    }
    else
    {
      button2.pressed = true;
    }
    if (bitRead(state, 2))
    {
      button3.pressed = false;
    }
    else
    {
      button3.pressed = true;
    }
  }
}

float readVoltage(uint8_t channel)
{
  // reads either the input of output voltage dividers
  // need to turn on the relevant voltage divider MOSFET
  // 0 = input; 1 = output voltage dividers
  float vUSB = 0.0;
  if (channel == 0)
  {
    digitalWrite(18, HIGH);                // turn on voltage pass through MOSFET
    vUSB = readADC(hspi, usbIN_vADC_CHNL); //  dummy run to set channel properly
    vUSB = readADC(hspi, usbIN_vADC_CHNL); //  go read ADC port
    digitalWrite(18, LOW);                 // turn off MOSFET
  }
  if (channel == 1)
  {
    digitalWrite(8, HIGH);                  // turn on voltage pass through MOSFET
    vUSB = readADC(hspi, usbOut_vADC_CHNL); // go read ADC port
    vUSB = readADC(hspi, usbOut_vADC_CHNL); // go read ADC port
    digitalWrite(8, LOW);                   // turn off MOSFET
  }
  return vUSB;
}

void writeSD(uint16_t buffer)
{
  // write buffer out to SD card - #### Needs more work below!
  Serial.println("SD start");
  File fileSD = SD.open("/uppData-1.csv");
  for (u16_t i = 0; i < (buffer); i++)
  {
  }
}

void listSDfiles(File dir, int numTabs)
{
  while (true)
  {
    File entry = dir.openNextFile();
    if (!entry)
    {
      dir.rewindDirectory();
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++)
      Serial.print('\t');
    Serial.print(entry.name());
    if (entry.isDirectory())
    {
      Serial.println("/");
      listSDfiles(entry, numTabs + 1);
    }
    else
    {
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}

void checkMode()
{
  // check to see if the Mode switch has been pressed
  if (buttonM)
  {
    while (digitalRead(bootMode_Pin) == 0)
    {
      // pause while the Boot/Mode switch is being held down
      delay(60);
    }
    buttonM = false;
    // the mode switch has been pressed.  Change modes
    mode++;
    buttonM = false;
    if (mode > sizeof(modeText) / sizeof(modeText[0]) - 1)
    {
      // cycle around to the beginning again
      mode = 0;
    }
  }
}

#ifdef wifi
void wifiEnable()
{
  if (wifiMode)
  {
    // Turn on WiFi, is we now have USB power and it's not yet running
    if (!wifiRunOnce)
    {
      // this can only run once
      Serial.println("Setting AP (Access Point)…");
      // Remove the password parameter, if you want the AP (Access Point) to be open
      WiFi.softAP(ssid, password, 1, 0, 1); // SSID, PWD, wifi channel, 0=display SSID, # of connections
      IPAddress myIP = WiFi.softAPIP();
      Serial.print("AP IP address: ");
      Serial.println(myIP);
      server.begin();
      Serial.println("WiFi:  ON");
      wifiRunOnce = true;
      // TODO: look at writing a State machine for the Loop area!
      // TODO: display simple webpage
    }
  }
}

void wifiDisable()
{
  // Turn off WiFi
  if (wifiEn)
    if (wifiRunOnce)
    {
      {
        WiFi.disconnect(true);
        WiFi.mode(WIFI_OFF);
        // esp_wifi_stop();
        Serial.println("WiFi:  OFF");
        wifiRunOnce = false;
      }
    }
}
#endif

void setup()
{
  Wire.begin(SDA, SCL);
  // Wire.setClock(400000);
  Serial.begin(115200);
  Serial.setTxTimeoutMs(0);
  delay(500); // wait till serial is online
  Serial.println("serial ready");
  // SD Card setup
  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);
  // Buzzer setup
  ledcSetup(BUZZPWM_CHANNEL, BUZZPWM_FREQ, BUZZPWM_RESOLUTION);
  ledcAttachPin(buzzerPin, BUZZPWM_CHANNEL);
  Serial.println("buzzer ready");
  soundBuzzer(200);
  soundBuzzer(200);
  // setup Power LED
  ledcSetup(LEDPWRPWM_CHANNEL, LEDPWRPWM_FREQ, LEDPWRPWM_RESOLUTION);
  ledcAttachPin(ledRunPin, LEDPWRPWM_CHANNEL);
  ledcWrite(LEDPWRPWM_CHANNEL, 10);
  Serial.println("0");
//  create SPI setup for new LCD
#ifdef SSD1327
  // mono 128x128 i2C OLED Setup
  u8g2.setBusClock(800000);
  // u8g2.setBusClock(400000);
  u8g2.begin();
  u8g2.setFlipMode(flipMode);
  u8g2.setFont(u8g2_font_6x10_mr);
  u8g2.setFontDirection(TEXT_ALIGN_LEFT);
  Serial.println("Screen = SSD1327 128x128");
#endif
#ifdef SSD1306
  // mono 128x64 i2c OLED Setup
  u8g2.begin();
  u8g2.setBusClock(400000);
  u8g2.setFlipMode(flipMode);
  u8g2.setFontDirection(TEXT_ALIGN_LEFT);
  u8g2.setFont(u8g2_font_6x10_mr);
  Serial.println("Screen = SSD1306 128x64");
#endif
#ifdef SSD1351
  // color 128x128 SPI OLED Setup
  gfx->begin();
  gfx->fillScreen(BLACK);
  gfx->setCursor(10, 0); // use to be 10, 10 so the Text started at bottom LH side.  Now it starts at top LH side
  gfx->setTextColor(RED);
  gfx->setTextSize(1);
  gfx->println("USB Power Profiler");
  gfx->setCursor(40, 10); // use to be 10, 10 so the Text started at bottom LH side.  Now it starts at top LH side
  gfx->setTextColor(GREEN);
  gfx->println(softwareVersion);
  delay(500);
#endif
  Serial.println("1");
  pinMode(sense1_EN, OUTPUT);
  digitalWrite(sense1_EN, LOW);
  pinMode(sense2_EN, OUTPUT);
  digitalWrite(sense2_EN, LOW);
  pinMode(sense3_EN, OUTPUT);
  digitalWrite(sense3_EN, LOW);
  pinMode(sense4_EN, OUTPUT);
  digitalWrite(sense4_EN, LOW);
  pinMode(voltIN_EN, OUTPUT);
  digitalWrite(voltIN_EN, LOW);
  pinMode(voltOUT_EN, OUTPUT);
  digitalWrite(voltOUT_EN, LOW);
  pinMode(curr_EN, OUTPUT);
  digitalWrite(curr_EN, HIGH);
  pinMode(PCA9535_INT_Pin, INPUT_PULLUP); // missing a 10K pullup, so try the internal pullup
  Serial.println("2");
  // setup PCA9535 GPIO Expander
  gpio.begin();             // use default address 0
  gpio.pinMode(0, OUTPUT);  // USB1_EN
  gpio.pinMode(1, OUTPUT);  // USB2_EN
  gpio.pinMode(2, INPUT);   // Calib_EN Switch
  gpio.pinMode(3, OUTPUT);  // cal1_EN
  gpio.pinMode(4, OUTPUT);  // cal2_EN
  gpio.pinMode(5, OUTPUT);  // cal3_EN
  gpio.pinMode(6, OUTPUT);  // cal4_EN
  gpio.pinMode(7, OUTPUT);  // cal5_EN
  gpio.pinMode(8, OUTPUT);  // cal6_EN
  gpio.pinMode(9, INPUT);   // bypass_EN Switch
  gpio.pinMode(10, OUTPUT); // VadjBypass_EN
  gpio.pinMode(11, OUTPUT); // Vadj_EN
  gpio.pinMode(12, OUTPUT); // USB_Out_EN
  gpio.pinMode(13, INPUT);  // SENSE_EN
  gpio.pinMode(14, OUTPUT); // Bypass_All_EN
  gpio.pinMode(15, OUTPUT); // PH-In_EN
  delay(300);
  Serial.println("3");
  // SD Card setup
  if (SD.begin(SD_CS))
  {
    Serial.println("SD card is present & ready");
    Serial.println("SD Card File Listing:");
    myFile = SD.open("/");
    listSDfiles(myFile, 0);
  }
  else
  {
    Serial.println("SD card missing or failure");
    while (1)
    {
      // display error msg to screen
      displayText(20, 40, RED, "SD Card missing!");
      delay(750);
      displayText(20, 40, BLACK, "SD Card missing!");
      delay(750);
      // wait here forever.  Get rid of this soon
    }
  }
  attachInterrupt(PCA9535_INT_Pin, PCA9535, FALLING);
  attachInterrupt(bootMode_Pin, modeSW, FALLING);
  Serial.println("3a");
  // rgbTestLED(); // make sure all are working (this is old V0.5.  V0.6 has a lot more LEDs)
  rotaryEncoder.begin(); // we have a GPIO error here, but the Encoder pins are correct!
  rotaryEncoder.setup(readEncoderISR);
  rotaryEncoder.setBoundaries(0, 100, circleValues); // minValue, maxValue, circleValues true|false (when max go to min and vice versa)
  // rotaryEncoder.setBoundaries(0, 255, circleValues); // minValue, maxValue, circleValues true|false (when max go to min and vice versa)
  rotaryEncoder.setEncoderValue(1);   // set the starting number
  rotaryEncoder.setAcceleration(150); // Acceleration 2-255; 0 or 1 disables Accel.
  Serial.println("4");
  RTclock.begin(); // initialise the RTC IC
  RTclock.cap_sel(CAP_SEL_12_5PF);
  Serial.println("5");
  if (setTimeEnable)
  {
    // writes the current time to the RTC IC, then pauses
    setTime(); //  Use this to set date/time. Need to edit function with correct time first. Could allow setting via encoder
    Serial.print("Time has been set: ");
    printTime();
    Serial.print("set setTimeEnable to false and upload again");
    displayText(10, 90, RED, "Time has been Set!");
    while (1)
    {
      // pause here
    }
  }
  Serial.println("6");
  // ADC128 initialisation
  hspi = new SPIClass(FSPI);                             // for ESP32-S3, you can only use FSPI or HSPI.  VSPI is not used here
  pinMode(HSPI_CS, OUTPUT);                              // HSPI SS, for ADC128 SPI ADC
  digitalWrite(HSPI_CS, HIGH);                           // pull ss/cs high to disable ADC128 IC
  hspi->begin(HSPI_SCLK, HSPI_MISO, HSPI_MOSI, HSPI_CS); // SCLK, MISO, MOSI, SS/CS

  // set and test all WS2812C LEDs
  FastLED.addLeds<LED_TYPE, RGB_PIN, COLOR_ORDER>(leds, NUM_RGBleds);
  FastLED.setBrightness(BRIGHTNESS);
  fill_solid(leds, NUM_RGBleds, CRGB::Red);
  FastLED.show();
  delay(300);
  fill_solid(leds, NUM_RGBleds, CRGB::Green);
  FastLED.show();
  delay(300);
  fill_solid(leds, NUM_RGBleds, CRGB::Blue);
  FastLED.show();
  delay(600);
  FastLED.clear();
  FastLED.show();
  Serial.println("7");
  OLED_Header();
#ifdef wifi
  // if (wifiMode)
  // {
  // Connect to Wi-Fi network with SSID and password
  Serial.println("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password, 1, 0, 1); // SSID, PWD, wifi chanel, 0=display SSID, # of connections
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  // Send web page with input fields to client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html, processor); });

  // Send a GET request to <ESP_IP>/get?input1=<inputMessage>
  server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request)
            {
    // char* inputMessage;
    // char* inputParam;
    String inputMessage;
    String inputParam;
    // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
    if (request->hasParam(PARAM_INPUT_1))
    {
      inputMessage = request->getParam(PARAM_INPUT_1)->value();
      inputParam = PARAM_INPUT_1;
    }
    // GET input2 value on <ESP_IP>/get?input2=<inputMessage>
    else if (request->hasParam(PARAM_INPUT_2))
    {
      inputMessage = request->getParam(PARAM_INPUT_2)->value();
      inputParam = PARAM_INPUT_2;
    }
    // GET input3 value on <ESP_IP>/get?input3=<inputMessage>
    else if (request->hasParam(PARAM_INPUT_3)) {
      inputMessage = request->getParam(PARAM_INPUT_3)->value();
      inputParam = PARAM_INPUT_3;
    }
    else
    {
      inputMessage = "No message sent";
      inputParam = "none";
    }
    Serial.print("*");
    Serial.print(inputMessage);
    Serial.println("*");
    Serial.print("*");
    Serial.print(inputParam);
    Serial.println("*");
    request->send(200, "text/html", "HTTP GET request sent to your ESP on input field (" + inputParam + ") with value: " + inputMessage + "<br><a href=\"/\">Return to Home Page</a>");
    if (inputParam == "datetime")
    {
      // this will input and check the datetime string thats being inputted from the webpage to make sure it's correctly formatted
      //  and then if ok, write the data into the RTC, and then update the webpage that it's been updated ok
      String webDateTime = inputMessage;
      Serial.print("Datetime field from the webpage: ");
      Serial.println(webDateTime);
      // char dateTime[] = {"2017-03-27 10:30:15"};  // example string
      if (webDateTime.length() != 19)
      {
        // notify webpage that there is a datetime string format error!
      }
      else
      {
        // we have a correct string format.  Go split it up and write out values to RTC
        // Split the string into substrings
        String strs[20];
        u8_t StringCount = 0;
        String strs1[20];
        u8_t StringCount1 = 0;
        String dow = "";
        while (webDateTime.length() > 0)
        {
          int index = webDateTime.indexOf('-');
          if (index == -1) // No space found
          {
            strs[StringCount++] = webDateTime;
            break;
          }
          else
          {
            strs[StringCount++] = webDateTime.substring(0, index);
            webDateTime = webDateTime.substring(index + 1);
          }
        }
        // manually force the date into the variable
        strs[StringCount++] = webDateTime.substring(0, 2); // manually add in the day field
        webDateTime = webDateTime.substring(3);            // get rid of day from string
        while (webDateTime.length() > 0)
        {
          int index = webDateTime.indexOf(':');
          if (index == -1) // No space found
          {
            strs[StringCount++] = webDateTime;
            break;
          }
          else
          {
            strs[StringCount++] = webDateTime.substring(0, index);
            webDateTime = webDateTime.substring(index + 1);
          }
          strs[StringCount++] = webDateTime; // manually add in the second field
        }
        // need a whole bunch of error checking here to somewhat verify correct data entry
        // NOTE: missing indexes below contain crappy data!
        Serial.println(strs[0]); // year
        Serial.println(strs[1]); // Month
        Serial.println(strs[3]); // Day
        Serial.println(strs[4]); // Hour
        Serial.println(strs[6]); // Minute
        Serial.println(strs[7]); // Second
        /*
        switch (dayOfWeek(strs[0].toInt(), strs[1].toInt(), strs[3].toInt()))
        {
        case 0:
          dow = "SUN";
          break;
        case 1:
          dow = "MON";
        case 2:
          dow = "TUE";
          break;
        case 3:
          dow = "WED";
          break;
        case 4:
          dow = "THU";
          break;
        case 5:
          dow = "FRI";
          break;
        case 6:
          dow = "SAT";
          break;
        }
      } */
      // now go store the datetime values into the RTC
      u8_t DoW = dayOfWeek(strs[0].toInt(), strs[1].toInt(), strs[3].toInt());
      Serial.print("DoW: ");
      Serial.println(DoW);
      RTclock.stopClock();
      RTclock.fillByYMD(strs[0].toInt(), strs[1].toInt(), strs[3].toInt()); // YYYY,MM,DD
      RTclock.fillByHMS(strs[4].toInt(), strs[6].toInt(), strs[7].toInt()); // hh,mm,ss
      RTclock.fillDayOfWeek(DoW);                                           // Friday
      RTclock.setTime();                                                    // write time to the RTC chip
      RTclock.startClock();                                                 // start the clock again
    }
    }
    if (inputParam == "ipaddress")
    {
      // convert string to ipaddress 
      int str_len = inputMessage.length() + 1;
      char str_array[str_len];
      // inputName = inputMessage.c_str();
      Serial.println(str_len);
      // str_array = inputMessage.c_str();
      inputMessage.toCharArray(clientIPAddress, str_len);
      // use strpy and then strcat into array that is long enough to hold everything
      // inputName = str_array;
      // inputName = strtok(str_array, " ");
    }
    Serial.print("Inputed IP Address: ");
    Serial.println(clientIPAddress); });
  server.onNotFound(notFound);
  server.begin();
#endif
  Serial.println("Setup Finished!");
  // testVoltages();  // use this to output a 1-127 LDO resistor/voltage out data plot
}

void loop()
{
  // set inital board states
  uint8_t reValue = 0;
  uint8_t pot100kValue = 1; // initially set the voltage limiting pot to be 50%
  uint8_t pot5kValue = 1;   // initially set the current limiting pot to be 100%
  uint8_t charPosition = 5; // base character position on display line

  bool reButtonPress = false;          // stores state of Rotary Encoder button press
  uint8_t previouspot100kValue = 1;    // stores previous value
  uint8_t previouspot5kValue = 1;      // stores previous value
  uint8_t previousCalibrationMode = 2; // stores previous value
  uint8_t previousSenseRes = 3;        // stores previous value
  uint8_t previousInputMode = 1;       // stores previous Input Mode value
  u8_t previousVoltPath = 1;           // stores previous value
  uint8_t spsMode = 0;                 // 0-5 index for the samples per second list
  float vUSB = 0.0;
  uint8_t InputMode = previousInputMode;     // update to current value
  senseRes = previousSenseRes;               // update to current value
  calibrationMode = previousCalibrationMode; // stores previous value
  Serial.println("set all path LEDs to block (red)");
  fill_solid(leds, NUM_RGBleds, CRGB::Red);
  FastLED.show();
  //  ##### insert RTC Config read here
  preferences.begin("upp", false); // open RTC Flash for reading
  u8_t firstUse = preferences.getUInt("firstuse", 99);
  if (firstUse == 1)
  {
    Serial.println("Found RTC Flash config data, go read it");
    previouspot100kValue = preferences.getUInt("Ppot100kValue", 0);
    previouspot5kValue = preferences.getUInt("Ppot5kValue", 0);
    previousCalibrationMode = preferences.getUInt("PCalMode", 0);
    previousSenseRes = preferences.getUInt("PSenseRes", 0);
    previousInputMode = preferences.getUInt("PInputMode", 0);
    previousVoltPath = preferences.getUInt("PVoltPath", 0);
    preferences.end();
    // serial print out all config data:
    Serial.print("firstuse: ");
    Serial.println(firstUse);
    Serial.print("Ppot100kValue: ");
    Serial.println(previouspot100kValue);
    Serial.print("Ppot5kValue: ");
    Serial.println(previouspot5kValue);
    Serial.print("PCalMode: ");
    Serial.println(previousCalibrationMode);
    Serial.print("PSenseRes: ");
    Serial.println(previousSenseRes);
    Serial.print("PInputMode: ");
    Serial.println(previousInputMode);
    Serial.print("PVoltPath: ");
    Serial.println(previousVoltPath);
    pot100kValue = previouspot100kValue;
    pot5kValue = previouspot5kValue;
    calibrationMode = previousCalibrationMode;
    senseRes = previousSenseRes;
    InputMode = previousInputMode;
  }
  else if (firstUse == 99)
  {
    // this is the first time this has been run, so store a default set of values to use next time
    Serial.println("RTC Flash config data not found, go create it");
    preferences.begin("upp", false); // open RTC Flash for writing
    preferences.putUInt("firstuse", 1);
    preferences.putUInt("Ppot100kValue", 1);
    preferences.putUInt("Ppot5kValue", 1);
    preferences.putUInt("PCalMode", 3);
    preferences.putUInt("PSenseRes", 3);
    preferences.putUInt("PInputMode", 1);
    preferences.putUInt("PVoltPath", 1);
    preferences.end();
  }
  // setup bopard according to last used config:
  Serial.println("Initalise the board config");
  enableUSB(previousInputMode);               // 0 = no Ports; 1 = USB Micro; 2 = USB C; 3=PH Connector
  enableVoltagePath(previousVoltPath);        // Vpath0 = non; Vpath1 = turn on Volt Adjust; Vpath2 = bypass Volt adjust; Vpath3 = bypass V & C paths
  enableCurrentSense(previousSenseRes);       // sense0 = None; sense1 = 100/20 ohm; sense2 = 2 ohm; sense3 = 0.2 ohm; sense4 = 0.05 ohm
  enableCalibration(previousCalibrationMode); //  Cal0 = None; Cal1 = 500mA; Cal2 = 100mA; Cal3 = 10mA; Cal4 = 1mA; Cal5 = 100uA; Cal6 = 10uA; Cal7 = USB/PH connector Out
  write_100K(previouspot100kValue);           // update digital 100K pot
  write_5K(previouspot5kValue);               // update digital 5K pot
  reButtonPress = true;                       // go update the screen
  printTime();                                // serial print the current time
  while (true)                                // Loop here forever
  {
    if (mode == 0)
    {
      // change to normal Run mode
      checkSwitches();
      OLED_Header();
      // test list pressed buttons:
      if (button1.pressed)
      {
        //  display current data to Serial Port.  1st line is datatime
        // Format: milisec, volts, current
        displayText(10, 118, RED, "Data-->SP");
        while (button1.pressed)
        {
          // TODO: SW1 should toggle on/off
          // stream out data to Serial Port
          Serial.printf("%d,%01.3f,%01.4f\n", micros(), readVoltageIn(hspi, 7), readSenseCurrent(hspi, senseRes) - Voffset);
          // Plot to TelePlot:
          // Serial.printf(">Volts:%d:%01.3f\n", micros(), readVoltageIn(hspi, 7));
          // Serial.printf(">Current:%d:%01.3f\n", micros(), readSenseCurrent(hspi, senseRes));
          checkSwitches();
        }
        printTime();
      }
      if (button2.pressed)
      {
        // write current data to SD card.
        displayText(30, 118, RED, "Data-->SD");
        // concoct a unique SD card CSV filename based on datetime stamp        RTclock.getTime();
        String fileName = "";
        RTclock.getTime();
        fileName = "/";
        // fileName += RTclock.year;
        fileName += RTclock.month;
        fileName += RTclock.dayOfMonth;
        fileName += RTclock.hour;
        fileName += RTclock.minute;
        fileName += RTclock.second;
        Serial.println(fileName);
        Serial.println(fileName.length());
        Serial.println("Writing to " + fileName + ".csv");
        // myFile = SD.open(fileName, FILE_WRITE);
        myFile = SD.open(fileName + ".csv", FILE_WRITE);
        RTclock.getTime();
        myFile.print(RTclock.year + 2000, DEC);
        myFile.print("-");
        myFile.print(RTclock.month, DEC);
        myFile.print("-");
        myFile.print(RTclock.dayOfMonth, DEC);
        myFile.print(" ");
        myFile.print(RTclock.hour, DEC);
        myFile.print(":");
        myFile.print(RTclock.minute, DEC);
        myFile.print(":");
        myFile.println(RTclock.second, DEC);
        // write out current settings:
        myFile.print("Settings - Input: ");
        myFile.print(inputMode[InputMode]);
        myFile.print("; Sense: ");
        myFile.print(sensePath[senseRes]);
        myFile.print("; Output: ");
        myFile.println(outputMode[calibrationMode]);
        // write out a CSV header
        myFile.println("Micros,Voltage,Current");
        // Format: microsec, volts, current
        while (button2.pressed)
        {
          // Store data to SD Card - open the file. Note that only one file can be open at a time,
          if (myFile)
          {
            myFile.print(micros());
            myFile.print(","); // comma
            myFile.print(readVoltageIn(hspi, 7));
            myFile.print(","); // comma
            myFile.println(readSenseCurrent(hspi, senseRes));
          }
          else
          {
            Serial.println("error opening " + fileName + ".csv");
          }
          checkSwitches();
        }
        myFile.close();
        printTime();
      }
      if (button3.pressed)
      {
        // when this is pressed, stream voltage and current data as fast as possible to the client PC via UDP packets.
        displayText(0, 118, WHITE, "Client:");
        displayText(50, 118, WHITE, clientIPAddress);
        // need a test here to check that we have a valid client IP address, otherwise, display an error msg.
        WiFiClient client; // setup a TCP client
        if (!client.connect(tcpAddress, tcpPort))
        {
          Serial.println("connection failed");
          return;
        }
        else
        {
          Serial.print("TCP connection created with: ");
          Serial.print(tcpAddress);
          Serial.print(" Port: ");
          Serial.print(tcpPort);
        }
        while (button3.pressed)
        {
          // need to add in TCP transmit function here
          /* This will send the data to the server */
          client.print(micros());
          client.print(","); // comma
          client.print(readVoltageIn(hspi, 7));
          client.print(","); // comma
          Serial.print(".");
          client.println(readSenseCurrent(hspi, senseRes));
          checkSwitches();
        }
        client.stop();
      }
      switch (menuState)
      {
      // update current menu value with new RE value
      case 1:
        // update  LDO voltage via AD5247 and MCP1827
        if (reButtonPress)
        {
          rotaryEncoder.setEncoderValue(previouspot100kValue);
          reButtonPress = false;
        }
        pot100kValue = rotaryEncoder.readEncoder();
        if (pot100kValue > 127)
        {
          pot100kValue = 127;
          rotaryEncoder.setEncoderValue(127);
        }
        else if (pot100kValue < 1)
        {
          pot100kValue = 1;
          rotaryEncoder.setEncoderValue(1);
        }
        break;
      case 2:
        // update output current via MIC2097 and MCP4019
        if (reButtonPress)
        {
          rotaryEncoder.setEncoderValue(previouspot5kValue);
          reButtonPress = false;
        }
        pot5kValue = rotaryEncoder.readEncoder();
        if (pot5kValue > 127)
        {
          pot5kValue = 127;
          rotaryEncoder.setEncoderValue(127);
        }
        else if (pot5kValue < 1)
        {
          pot5kValue = 1;
          rotaryEncoder.setEncoderValue(1);
        }
        break;
      case 3:
        // update sense current path
        // sense0 = none; sense1 = 100/20 ohm; sense2 = 2 ohm; sense3 = 0.2 ohm; sense4 = 0.05 ohm
        if (reButtonPress)
        {
          rotaryEncoder.setEncoderValue(previousSenseRes);
          reButtonPress = false;
        }
        senseRes = rotaryEncoder.readEncoder();
        if (senseRes > 4)
        {
          senseRes = 4;
          rotaryEncoder.setEncoderValue(4);
        }
        else if (senseRes < 0)
        {
          senseRes = 0;
          rotaryEncoder.setEncoderValue(0);
        }
        break;
      case 4:
        // select input path: In0=none, In1=USBmicro,In2=USBC,In3=JST - default In1
        if (reButtonPress)
        {
          rotaryEncoder.setEncoderValue(previousInputMode);
          reButtonPress = false;
        }
        InputMode = rotaryEncoder.readEncoder();
        if (InputMode > 3)
        {
          InputMode = 3;
          rotaryEncoder.setEncoderValue(3);
        }
        else if (InputMode < 0)
        {
          InputMode = 0;
          rotaryEncoder.setEncoderValue(0);
        }
        break;
      case 5:
        // select Calibration section, or USB out, or none
        //  Cal0 = None; Cal1 = 500mA; Cal2 = 100mA; Cal3 = 10mA; Cal4 = 1mA; Cal5 = 100uA; Cal6 = 10uA; Cal7 = USB/PH connector Out
        if (reButtonPress)
        {
          rotaryEncoder.setEncoderValue(previousCalibrationMode);
          reButtonPress = false;
        }
        calibrationMode = rotaryEncoder.readEncoder();
        if (calibrationMode > 8)
        {
          calibrationMode = 4;
          rotaryEncoder.setEncoderValue(4);
        }
        else if (calibrationMode < 0)
        {
          calibrationMode = 0;
          rotaryEncoder.setEncoderValue(0);
        }
        break;
      }
      if (rotaryEncoder.isEncoderButtonClicked())
      {
        Serial.println("RE switch pressed");
        // cycle around avaliable menu states: 1) LDO Voltage; 2) Output current; 3) Sense Resistor; 4) Input path; 5) output mode
        switch (menuState)
        {
        case 1:
          menuState = 2;
          // update voltage LDO 100K pot
          write_100K(pot100kValue);
          previouspot100kValue = pot100kValue;
          reButtonPress = true;
          break;
        case 2:
          menuState = 3;
          // update current digital 5K pot
          write_5K(pot5kValue);
          previouspot5kValue = pot5kValue;
          reButtonPress = true;
          break;
        case 3:
          menuState = 4;
          // update Sense Path ADC/Resistors
          Serial.print("send Sense path data: ");
          Serial.println(senseRes);
          previousSenseRes = senseRes;
          enableCurrentSense(senseRes); // sense0 = None; sense1 = 100/20 ohm; sense2 = 2 ohm; sense3 = 0.2 ohm; sense4 = 0.05 ohm
          Serial.println("Sense path updated!");
          reButtonPress = true;
          break;
        case 4:
          menuState = 5;
          // select input path mode: In0=none, In1=USBmicro,In2=USBC,In3=JST - default In1
          // set selected Input mode:
          Serial.println("reconfigure Input Path");
          previousInputMode = InputMode;
          reButtonPress = true;
          enableUSB(InputMode); // In0=none, In1=USBmicro, In2=USBC, In3=JST
          break;
        case 5:
          menuState = 6;
          // modes: 0=nothing; 1=USB out; 2=100mA load; 3=500mA load; 4=1A load
          // set selected output mode:
          Serial.println("reconfigure paths for new Calibration setup");
          previousCalibrationMode = calibrationMode;
          reButtonPress = true;
          enableCalibration(calibrationMode); // Cal0 = None; Cal1 = 500mA; Cal2 = 100mA; Cal3 = 10mA; Cal4 = 1mA, Cal5 = 100uA, Cal6 = 10uA, Cal7 = USB
          break;
        case 6:
          // set Samples per Second and recording length
          menuState = 1;
          // startRecording(spsMode);
          break;
        }
        // SoundBuzzer();
      }
      // update display to show new state and values
      uint8_t vOffset = 12;
      uint8_t hOffset = 15;
      uint16_t textColor = BLUE;  // current text font Color
      uint16_t highColor = WHITE; // current text font Color
      switch (menuState)
      {
      case 1:
#ifdef SSD1351
        displayText(10, 2 * vOffset, textColor, "Voltage:");
        displayInt(hOffset * charPosition, 2 * vOffset, highColor, pot100kValue);
        displayText(1, 3 * vOffset, textColor, "Current:");
        displayInt(hOffset * charPosition, 3 * vOffset, textColor, pot5kValue);
        displayText(1, 4 * vOffset, textColor, "Sense:");
        displayText(hOffset * charPosition, 4 * vOffset, textColor, sensePath[senseRes]);
        displayText(1, 5 * vOffset, textColor, "Input:");
        displayText(hOffset * charPosition, 5 * vOffset, textColor, inputMode[InputMode]);
        displayText(1, 6 * vOffset, textColor, "Output:");
        displayText(hOffset * charPosition, 6 * vOffset, textColor, outputMode[calibrationMode]);
        displayText(1, 7 * vOffset, textColor, "SPS/Lgth:");
        displayText(hOffset * charPosition, 7 * vOffset, textColor, samplesPerSec[spsMode]);
#endif
#ifdef SSD1327
        u8g2.drawStr(10, 2 * vOffset, "Voltage: ");
        u8g2.setCursor(13 * charPosition, 2 * vOffset);
        u8g2.print(pot100kValue);
        u8g2.drawStr(1, 3 * vOffset, "Current: ");
        u8g2.setCursor(16 * charPosition, 3 * vOffset);
        u8g2.print(pot5kValue);
        u8g2.drawStr(1, 4 * vOffset, "Sense: ");
        u8g2.setCursor(16 * charPosition, 4 * vOffset);
        u8g2.print(sensePath[senseRes]);
        u8g2.drawStr(1, 5 * vOffset, "Output:");
        u8g2.setCursor(16 * charPosition, 5 * vOffset);
        u8g2.print(outputMode[calibrationMode]);
        u8g2.drawStr(1, 6 * vOffset, "SPS/Lgth:");
        u8g2.setCursor(16 * charPosition, 6 * vOffset);
        u8g2.print(samplesPerSec[spsMode]);
#endif
        break;
      case 2:
#ifdef SSD1351
        displayText(1, 2 * vOffset, textColor, "Voltage:");
        displayInt(hOffset * charPosition, 2 * vOffset, textColor, pot100kValue);
        displayText(10, 3 * vOffset, textColor, "Current:");
        displayInt(hOffset * charPosition, 3 * vOffset, highColor, pot5kValue);
        displayText(1, 4 * vOffset, textColor, "Sense:");
        displayText(hOffset * charPosition, 4 * vOffset, textColor, sensePath[senseRes]);
        displayText(1, 5 * vOffset, textColor, "Input:");
        displayText(hOffset * charPosition, 5 * vOffset, textColor, inputMode[InputMode]);
        displayText(1, 6 * vOffset, textColor, "Output:");
        displayText(hOffset * charPosition, 6 * vOffset, textColor, outputMode[calibrationMode]);
        displayText(1, 7 * vOffset, textColor, "SPS/Lgth:");
        displayText(hOffset * charPosition, 7 * vOffset, textColor, samplesPerSec[spsMode]);
#endif
#ifdef SSD1327
        u8g2.drawStr(1, 2 * vOffset, "Voltage: ");
        u8g2.setCursor(13 * charPosition, 2 * vOffset);
        u8g2.print(pot100kValue);
        u8g2.drawStr(10, 3 * vOffset, "Current: ");
        u8g2.setCursor(16 * charPosition, 3 * vOffset);
        u8g2.print(pot5kValue);
        u8g2.drawStr(1, 4 * vOffset, "Sense: ");
        u8g2.setCursor(16 * charPosition, 4 * vOffset);
        u8g2.print(sensePath[senseRes]);
        u8g2.drawStr(1, 5 * vOffset, "Output:");
        u8g2.setCursor(16 * charPosition, 5 * vOffset);
        u8g2.print(outputMode[calibrationMode]);
        u8g2.drawStr(1, 6 * vOffset, "SPS/Lgth:");
        u8g2.setCursor(16 * charPosition, 6 * vOffset);
        u8g2.print(samplesPerSec[spsMode]);
#endif
        break;
      case 3:
#ifdef SSD1351
        displayText(1, 2 * vOffset, textColor, "Voltage:");
        displayInt(hOffset * charPosition, 2 * vOffset, textColor, pot100kValue);
        displayText(1, 3 * vOffset, textColor, "Current:");
        displayInt(hOffset * charPosition, 3 * vOffset, textColor, pot5kValue);
        displayText(10, 4 * vOffset, textColor, "Sense:");
        displayText(hOffset * charPosition, 4 * vOffset, highColor, sensePath[senseRes]);
        displayText(1, 5 * vOffset, textColor, "Input:");
        displayText(hOffset * charPosition, 5 * vOffset, textColor, inputMode[InputMode]);
        displayText(1, 6 * vOffset, textColor, "Output:");
        displayText(hOffset * charPosition, 6 * vOffset, textColor, outputMode[calibrationMode]);
        displayText(1, 7 * vOffset, textColor, "SPS/Lgth:");
        displayText(hOffset * charPosition, 7 * vOffset, textColor, samplesPerSec[spsMode]);
#endif
#ifdef SSD1327
        u8g2.drawStr(1, 2 * vOffset, "Voltage: ");
        u8g2.setCursor(13 * charPosition, 2 * vOffset);
        u8g2.print(pot100kValue);
        u8g2.drawStr(1, 3 * vOffset, "Set Curr: ");
        u8g2.setCursor(16 * charPosition, 3 * vOffset);
        u8g2.print(pot5kValue);
        u8g2.drawStr(10, 4 * vOffset, "Sense: ");
        u8g2.setCursor(16 * charPosition, 4 * vOffset);
        u8g2.print(sensePath[senseRes]);
        u8g2.drawStr(1, 5 * vOffset, "Output:");
        u8g2.setCursor(16 * charPosition, 5 * vOffset);
        u8g2.print(outputMode[calibrationMode]);
        u8g2.drawStr(1, 6 * vOffset, "SPS/Lgth:");
        u8g2.setCursor(16 * charPosition, 6 * vOffset);
        u8g2.print(samplesPerSec[spsMode]);
#endif
        break;
      case 4:
#ifdef SSD1351
        displayText(1, 2 * vOffset, textColor, "Voltage:");
        displayInt(hOffset * charPosition, 2 * vOffset, textColor, pot100kValue);
        displayText(1, 3 * vOffset, textColor, "Current:");
        displayInt(hOffset * charPosition, 3 * vOffset, textColor, pot5kValue);
        displayText(1, 4 * vOffset, textColor, "Sense:");
        displayText(hOffset * charPosition, 4 * vOffset, textColor, sensePath[senseRes]);
        displayText(10, 5 * vOffset, textColor, "Input:");
        displayText(hOffset * charPosition, 5 * vOffset, highColor, inputMode[InputMode]);
        displayText(1, 6 * vOffset, textColor, "Output:");
        displayText(hOffset * charPosition, 6 * vOffset, textColor, outputMode[calibrationMode]);
        displayText(1, 7 * vOffset, textColor, "SPS/Lgth:");
        displayText(hOffset * charPosition, 7 * vOffset, textColor, samplesPerSec[spsMode]);
#endif
#ifdef SSD1327
        u8g2.drawStr(1, 2 * vOffset, "Voltage: ");
        u8g2.setCursor(13 * charPosition, 2 * vOffset);
        u8g2.print(pot100kValue);
        u8g2.drawStr(1, 3 * vOffset, "Set Curr: ");
        u8g2.setCursor(16 * charPosition, 3 * vOffset);
        u8g2.print(pot5kValue);
        u8g2.drawStr(1, 4 * vOffset, "Sense: ");
        u8g2.setCursor(16 * charPosition, 4 * vOffset);
        u8g2.print(sensePath[senseRes]);
        u8g2.drawStr(10, 5 * vOffset, "Output:");
        u8g2.setCursor(16 * charPosition, 5 * vOffset);
        u8g2.print(outputMode[calibrationMode]);
        u8g2.drawStr(1, 6 * vOffset, "SPS/Lgth:");
        u8g2.setCursor(16 * charPosition, 6 * vOffset);
        u8g2.print(samplesPerSec[spsMode]);
#endif
        break;
      case 5:
#ifdef SSD1351
        displayText(1, 2 * vOffset, textColor, "Voltage:");
        displayInt(hOffset * charPosition, 2 * vOffset, textColor, pot100kValue);
        displayText(1, 3 * vOffset, textColor, "Current:");
        displayInt(hOffset * charPosition, 3 * vOffset, textColor, pot5kValue);
        displayText(1, 4 * vOffset, textColor, "Sense:");
        displayText(hOffset * charPosition, 4 * vOffset, textColor, sensePath[senseRes]);
        displayText(1, 5 * vOffset, textColor, "Input:");
        displayText(hOffset * charPosition, 5 * vOffset, textColor, inputMode[InputMode]);
        displayText(10, 6 * vOffset, textColor, "Output:");
        displayText(hOffset * charPosition, 6 * vOffset, highColor, outputMode[calibrationMode]);
        displayText(1, 7 * vOffset, textColor, "SPS/Lgth:");
        displayText(hOffset * charPosition, 7 * vOffset, textColor, samplesPerSec[spsMode]);
#endif
#ifdef SSD1327
        u8g2.drawStr(1, 2 * vOffset, "Voltage: ");
        u8g2.setCursor(13 * charPosition, 2 * vOffset);
        u8g2.print(pot100kValue);
        u8g2.drawStr(1, 3 * vOffset, "Set Curr: ");
        u8g2.setCursor(16 * charPosition, 3 * vOffset);
        u8g2.print(pot5kValue);
        u8g2.drawStr(1, 4 * vOffset, "Sense: ");
        u8g2.setCursor(16 * charPosition, 4 * vOffset);
        u8g2.print(sensePath[senseRes]);
        u8g2.drawStr(10, 5 * vOffset, "Output:");
        u8g2.setCursor(16 * charPosition, 5 * vOffset);
        u8g2.print(outputMode[calibrationMode]);
        u8g2.drawStr(1, 6 * vOffset, "SPS/Lgth:");
        u8g2.setCursor(16 * charPosition, 6 * vOffset);
        u8g2.print(samplesPerSec[spsMode]);
#endif
        break;
      case 6:
#ifdef SSD1351
        displayText(1, 2 * vOffset, textColor, "Voltage:");
        displayInt(hOffset * charPosition, 2 * vOffset, textColor, pot100kValue);
        displayText(10, 3 * vOffset, textColor, "Current:");
        displayInt(hOffset * charPosition, 3 * vOffset, textColor, pot5kValue);
        displayText(1, 4 * vOffset, textColor, "Sense:");
        displayText(hOffset * charPosition, 4 * vOffset, textColor, sensePath[senseRes]);
        displayText(1, 5 * vOffset, textColor, "Input:");
        displayText(hOffset * charPosition, 5 * vOffset, textColor, inputMode[InputMode]);
        displayText(1, 6 * vOffset, textColor, "Output:");
        displayText(hOffset * charPosition, 6 * vOffset, textColor, outputMode[calibrationMode]);
        displayText(10, 7 * vOffset, textColor, "SPS/Lgth:");
        displayText(hOffset * charPosition, 7 * vOffset, highColor, samplesPerSec[spsMode]);
#endif
#ifdef SSD1327
        u8g2.drawStr(1, 2 * vOffset, "Set Volts: ");
        u8g2.setCursor(13 * charPosition, 2 * vOffset);
        u8g2.print(pot100kValue);
        u8g2.drawStr(1, 3 * vOffset, "set Curr: ");
        u8g2.setCursor(16 * charPosition, 3 * vOffset);
        u8g2.print(pot5kValue);
        u8g2.drawStr(1, 4 * vOffset, "Sense: ");
        u8g2.setCursor(16 * charPosition, 4 * vOffset);
        u8g2.print(sensePath[senseRes]);
        u8g2.drawStr(1, 5 * vOffset, "Output:");
        u8g2.setCursor(16 * charPosition, 5 * vOffset);
        u8g2.print(outputMode[calibrationMode]);
        u8g2.drawStr(10, 6 * vOffset, "SPS/Lgth:");
        u8g2.setCursor(16 * charPosition, 6 * vOffset);
        u8g2.print(samplesPerSec[spsMode]);
#endif
        break;
      default:
        break;
      }
      checkMode();
    }
    while (mode == 1)
    {
      // change to and display setup mode. 1) WiFi on/Off; 2) various current paths; 3) 3 User switches - change states/modes/uses; 4) external trigger modes
      // Would be nicer to use the rotery encoder like the main menu.  Could make it longer.
      checkMode();
      gfx->fillScreen(BLACK); // is there a better way to clear the screen?
      displayText(3, 0, GREEN, "Setup 1:");
      gfx->drawLine(1, 8, DISPLAY_WIDTH, 8, RED);
      Serial.print("Mode: ");
      Serial.println(modeText[mode]);

      displayText(5, 20, GREEN, "1) WiFi: ");
      displayText(5, 30, GREEN, "2) auto Input: ");
      displayText(5, 40, GREEN, "3) Test RGB LEDs: ");
      displayText(2, 60, WHITE, "Toggle options via switchs");
      /*
      bool wifiMode = false;                                   // tracks wifi on/off mode/status
      bool autoInputMode = false;                              // tracks if auto power input is on/off
      bool rgbTest = false;                                    // tracks if the RGB test is running or not (on/off)
      */
      checkSwitches();
      checkMode();
      if (button1.pressed)
      {
        // toggle WiFi status
        wifiMode = !wifiMode;
      }

      if (button2.pressed)
      {
        // toggle Power Input mode: manual vs. auto
        autoInputMode = !autoInputMode;
      }

      if (button3.pressed)
      {
        // cycle the RGB LEDs with color!
        rgbTest = !rgbTest;
      }
      if (wifiMode)
      {
        // turn on WiFi
        displayText(110, 20, WHITE, "ON");
#ifdef wifi
        wifiEn = true; // can't I just use wifiMode instead of wifiEn?
        // go turn on WiFi
        wifiEnable(); // note: this will only run once
        IPAddress myIP = WiFi.softAPIP();
        displayText(1, 80, BLUE, "IP add:");
        displayText(50, 80, WHITE, myIP.toString().c_str());
        // Serial.println("calling IP Address function");
        // wifiRunOnce = true;
#endif
        delay(50); // wait a bit so we can see the new data, before clearing the screen
      }
      else
      {
        // turn off WiFi
        wifiEn = false;
        displayText(110, 20, BLACK, "ON");
        displayText(110, 20, WHITE, "OFF");
#ifdef wifi
        wifiDisable(); // NOTE: this will only run once
#endif
        delay(50); // wait a bit so we can see the new data, before clearing the screen
      }
      if (autoInputMode)
      {
        // turn on Auto Input Mode
        displayText(110, 30, BLACK, "OFF");
        displayText(110, 30, WHITE, "ON");
        delay(50); // wait a bit so we can see the new data, before clearing the screen
      }
      else
      {
        // turn off Auto Input Mode
        displayText(110, 30, BLACK, "ON");
        displayText(110, 30, WHITE, "OFF");
        delay(50); // wait a bit so we can see the new data, before clearing the screen
      }
      if (rgbTest)
      {
        // run the RGB LED test
        displayText(110, 40, WHITE, "ON");
        for (u8_t i = 0; i < NUM_RGBleds; i++)
        {
          // cycle through boards RGB LEDs
          leds[i] = CRGB::Red;
          delay(50); // wait a bit so we can see the new data, before clearing the screen
          FastLED.show();
        }
        for (u8_t i = 0; i < NUM_RGBleds; i++)
        {
          // cycle through boards RGB LEDs
          leds[i] = CRGB::Green;
          delay(50); // wait a bit so we can see the new data, before clearing the screen
          FastLED.show();
        }
        for (u8_t i = 0; i < NUM_RGBleds; i++)
        {
          // cycle through boards RGB LEDs
          leds[i] = CRGB::Blue;
          delay(50); // wait a bit so we can see the new data, before clearing the screen
          FastLED.show();
        }
        delay(250); // wait a bit so we can see the new data, before clearing the screen
      }
      else
      {
        // stop the RGB LED test
        displayText(110, 40, WHITE, "OFF");
        for (u8_t i = 0; i < NUM_RGBleds; i++)
        {
          // cycle through boards RGB LEDs.  #### How to set them back to what they where before run the LED test?  Kind of need to re-run the board init again?
          leds[i] = CRGB::Black;
          FastLED.show();
        }
        // fill_solid(leds, NUM_RGBleds, CRGB::BLACK);
        // FastLED.show();
        delay(50); // wait a bit so we can see the new data, before clearing the screen
      }
      delay(150); // wait a bit so we can see the new data, before clearing the screen
    }

    while (mode == 2)
    {
      // this mode could be to handle activation via an external trigger via J6
      // should never get to this point.  Without this though, the MCU will crash-reboot if it gets here via a programming error!!
      checkMode();
      gfx->fillScreen(BLACK); // is there a better way to clear the screen?
      displayText(3, 0, GREEN, "Error:");
      gfx->drawLine(1, 8, DISPLAY_WIDTH, 8, RED);
      displayText(10, 20, RED, "MODE ERROR: ");
      displayInt(60, 20, WHITE, mode);
      Serial.print("Mode: ");
      Serial.println(modeText[mode]);
    }
    // printTime();                 // printing this out to Serial UART so I can track RTC time diff. over time
    /*
    // NOTE: this is a single voltage read, ie: it doesn't do a multi avg read.
    float vUSB = readVoltage(0); // 0 = Input; 1 = Output divider voltages

    Serial.print("USB Input voltage: ");
    Serial.println(vUSB);
    vUSB = readVoltage(1); // 0 = Input; 1 = Output divider voltages
    Serial.print("USB Output voltage: ");
    Serial.println(vUSB);
    */
    delay(150); // wait a bit so we can see the new data, before clearing the screen. Will be fixed by doing in-line updates to text.
  }
}
