# USB Power Profiler 
  
This project was inspired by the Nordic Power Profiler 2 device:  
[https://www.nordicsemi.com/Products/Development-hardware/Power-Profiler-Kit-2](https://www.nordicsemi.com/Products/Development-hardware/Power-Profiler-Kit-2) as a challange to have a go at making my own low current measurement board.
  
This aims to be able to profile USB power measurements down into the micro amp range.  Via the rotary encoder, you can adjust the measurement range, voltage output 1.8V to 5V and limit the usable current output from 100mA to 1A.  There are six onboard calibration loads: 10uA to 500mA.  
  
For more documentation visit [https://gitlab.com/parkview/usb_power_measurement_board](https://gitlab.com/parkview/usb_power_measurement_board)
  
## Hardware Features:
---------
#### Version 0.7 Features: 

* ESP32-S3 based microcontroller 
* four switched current measurement sections
* audio buzzer for notifications
* lots of RGB LEDs to show sections and path statuses
* incorporates a calibrated Dummy Load resistor, so we can run an internal current test over a range of resistances: 500mA, 100mA, 10mA, 1mA, 100uA, 10uA 
* switched internal power paths RU30L30 MOSFETs
* protect against a 5V short circuit!
* SD card socket for local data collection
* Real Time Controller (RTC) to record session datetime stamped data
* switchable USB C, USB Micro and JST-PH connector power inputs
* switchable USB A and JST-PH outputs
* output voltage short circuit protection
* SSD1351 color SPI OLED display 128x128px
* voltage and current adjustments via rotary encoder and onboard menu
* board powered from a seperate USB power supply 
  

## Available Software 
---------------
![Gitlab URL](images/UPP-QR_URL.png){ align=right : style="height:100px;width:100px"}
#### Arduino:
* `V0.7-ESP32-S3-WROOM-SPI_SSD1351_OLED-Test`  run this code to test the v0.7 PCB and display
* `V0.7-ESP32-S3-WROOM` experimental code to run the board. SD card hasn't been tested yet

![UPP v0.7](images/UPP_v0.7.jpg){ align=left : style="height:250px;width:250px"}
