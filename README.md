# USB Power Profiler
----------
   
This project was inspired by the Nordic Power Profiler 2 device:  
https://www.nordicsemi.com/Products/Development-hardware/Power-Profiler-Kit-2 as a challange to have a go at
making my own low current measurement board.  
  
This aims to be able to profile USB power measurements down into the micro amp range. Via the rotary encoder, you
can adjust the measurement range, voltage output 1.8V to 5V and limit the usable current output from 100mA to 1A.
There are six onboard calibration loads: 10uA to 500mA.  
For more documentation visit: https://gitlab.com/parkview/usb_power_measurement_board
A bit of a project journey: [https://forum.swmakers.org/viewtopic.php?f=9&t=2487](https://forum.swmakers.org/viewtopic.php?f=9&t=2487)  
  
------------
## Hardware Features:
### Version 0.7 Features:
* ESP32-S3 based microcontroller  
* four switched current measurement sections via INA180 current sense amplifiers and ADC128 SPI ADC
* audio buzzer for notifications  
* lots of WS2812 2mm RGB LEDs to show sections and path statuses  
* incorporates a calibrated Dummy Load resistor, so we can run an internal current test over a range of resistances: 500mA, 100mA, 10mA, 1mA, 100uA, 10uA  
* switched internal power paths via RU30L30 MOSFETs  
* adjustable USB voltage outputi via MCP1827, from 1.7V to 5V
* SD card socket for local data collection  
* PCF85063 based Real Time Controller (RTC) to record session datetime stamped data  
* switchable USB C, USB Micro and JST-PH connector power inputs  
* switchable USB A and JST-PH outputs  
* MIC2097 based output voltage short circuit protection  
* SPI SSD1351 color OLED display 128x128px  
* voltage and current adjustments via rotary encoder and display menu  
* board powered from a seperate USB power supply  
* WiFi configerable AP webpage  
  
## Software:

### Arduino
• `V0.7-ESP32-S3-WROOM-SPI_SSD1351_OLED-Test` -  run this code to test the v0.7 PCB and display  
• `V0.7-ESP32-S3-WROOM` - experimental code to run the board. SD card hasn't been tested yet  
  
